﻿using Xamarin.Forms;

namespace Dinkkkiia.WorkandPlay
{
    public partial class OldApp : Application
    {
        public OldApp()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
