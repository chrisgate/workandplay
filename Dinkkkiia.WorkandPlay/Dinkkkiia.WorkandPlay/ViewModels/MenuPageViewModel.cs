﻿using System.Collections.ObjectModel;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Common.Views;
using Dinkkkiia.WorkandPlay.Models;
using Dinkkkiia.WorkandPlay.Views;
using GalaSoft.MvvmLight.Command;
using Settings = Dinkkkiia.WorkandPlay.Views.Settings;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
	public class MenuPageViewModel : BaseViewModel
	{
		/*
         * Define Fields
         */
		// TODO: this is a good place to define services that will be initialized or injected in the constructor

		/*
         * Define Properties
         */
		private ObservableCollection<NavigationMenuItem> _menuItems = new ObservableCollection<NavigationMenuItem>();
		public ObservableCollection<NavigationMenuItem> MenuItems
		{
			get
			{
				return _menuItems;
			}
			set
			{
				Set(() => MenuItems, ref _menuItems, value);
			}
		}

		private NavigationMenuItem _selectedItem;
		public NavigationMenuItem SelectedItem
		{
			get
			{
				return _selectedItem;
			}
			set
			{
				Set(() => SelectedItem, ref _selectedItem, value);
				if (_selectedItem == null)
					return;
				this.SelectItemCommand.Execute(value);
				SelectedItem = null;
				Xamarin.Forms.MessagingCenter.Send(this, "CLOSE_MENU");
			}
		}

		/*
         * Define Commands
         */
		public RelayCommand<NavigationMenuItem> SelectItemCommand { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:MvvmLight.ViewModels.MenuPageViewModel"/> class.
        /// </summary>
        /// <param name="navigationService">Navigation service.</param>
		public MenuPageViewModel(INavigationService navigationService) : base(navigationService)
		{
            this.Title = "Utilities In The Cloud";

			// Commands
			SelectItemCommand = new RelayCommand<NavigationMenuItem>((s) => Navigate(s));

			Initialize();
		}

		/* 
         * Define Methods
         */
		private void Initialize()
		{
		    MenuItems.Add(new NavigationMenuItem()
		    {
		        Key = "Home",
		        Title = "Home",
		        Image = "home.png"
		    });
            //			MenuItems.Add(new NavigationMenuItem()
            //			{
            //				Key = "Work Sheet",
            //				Title = "WorkSheet",
            //                Image = "home.png"
            //			});
//            MenuItems.Add(new NavigationMenuItem()
//			{
//				Key = "SportGround",
//				Title = "SportGround",
//                Image = "home.png"
//			});
//            MenuItems.Add(new NavigationMenuItem()
//			{
//				Key = "Arena",
//				Title = "Arena",
//                Image = "home.png"
//			});
           

			MenuItems.Add(new NavigationMenuItem()
			{
				Key = "Settings",
				Title = "Settings",
                Image = "settings.png"
			});
//			MenuItems.Add(new NavigationMenuItem()
//			{
//				Key = "SettingsPage",
//				Title = "Custom Settings ",
//                Image = "settings.png"
//			});
            MenuItems.Add(new NavigationMenuItem()
			{
				Key = "Inventory",
				Title = "Inventory",
                Image = "settings.png"
			});
            MenuItems.Add(new NavigationMenuItem()
			{
				Key = "ReadWritePage",
				Title = "Read and Write Tags",
                Image = "settings.png"
			});
            MenuItems.Add(new NavigationMenuItem()
			{
				Key = "Connect",
				Title = "Connection Manager",
                Image = "settings.png"
			});
            MenuItems.Add(new NavigationMenuItem()
			{
				Key = "Reader",
				Title = "Reader Manager",
                Image = "settings.png"
			});
		}

		private void Navigate(NavigationMenuItem args)
		{
			var navKey = this._navigationService.CurrentPageKey;

			switch (args.Key)
			{
				case ("Home"):
                    navKey = nameof(HomePage);
					break;
//                case ("SportGround"):
//                    navKey = nameof(Views.SportGround);
//					break;
//                case ("Arena"):
//                    navKey = nameof(Views.Arena);
//					break;
                case ("Settings"):
                    navKey = nameof(Settings);
					break;
//                case ("SettingsPage"):
//                    navKey = nameof(Views.SettingsPage);
//					break;
                case ("Inventory"):
                    navKey = nameof(InventoryPage);
					break;
                case ("ReadWritePage"):
                    navKey = nameof(ReadWritePage);
					break;
                case ("Connect"):
                    navKey = nameof(ConnectPage);
					break;
                case ("Reader"):
                    navKey = nameof(ReaderPage);
					break;
				default:
					this._navigationService.GoBackToRoot();
                    return;
			}

			// Only navigate if it's a different page
			if (navKey != this._navigationService.CurrentPageKey)
				this._navigationService.NavigateTo(navKey);
		}
	}
}
