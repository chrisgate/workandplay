﻿//-----------------------------------------------------------------------
// <copyright file="ViewModelLocator.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System.Diagnostics;
using Dinkkkiia.WorkandPlay.Common.Models;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Models;
using Dinkkkiia.WorkandPlay.Services;
using GalaSoft.MvvmLight.Ioc;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Extensions;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    /// <summary>
    /// Configures the service container for the application
    /// </summary>
    public class OldViewModelLocator
    {
        /// <summary>
        /// The service container
        /// </summary>
        private SimpleIoc container;

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class
        /// </summary>
        public OldViewModelLocator()
        {
            Debug.WriteLine("OldViewModelLocator");
            // we are use MVVM light as our IOC container and therefore service provider
            // Lets sets the Default singleton instance as the current ServiceLocator
            //SimpleIoc.Default.SetLocatorProvider(() => SimpleIoc.Default);

            // now lets tell the service locator how to create our view models
            this.container = SimpleIoc.Default;
            //if (UseDesignData)
            //{
            //    //if (!this.container.IsRegistered<INavigationService>())
            //    //{
            //    //    this.container.Register<INavigationService, DesignNavigationService>();
            //    //}

            //    this.container.Register<IFlowersService, DesignFlowersService>();
            //}
            //else
            //{
            //    this.container.Register<IFlowersService, FlowersService>();
            //}

            this.container.Register<IReaderConnectionManager>(() => { return new ReaderConnectionManager(); });

            this.container.Register<IPeopleService, PeopleService>();
            this.container.Register<ConnectViewModel>();
            this.container.Register<ReaderViewModel>();
            this.container.Register<InventoryViewModel>();
            this.container.Register<HomeViewModel>();
            this.container.Register<ReadWriteViewModel>();
            this.container.Register<SettingsViewModel>();
            this.container.Register<LicenceViewModel>();
           // this.container.Register<CheckInOutViewModel>();

            this.container.Register<ISettings>(() => { return new Settings(); });

            this.container.Register<SwitchAsynchronousResponder>();

            this.container.Register<IAsciiCommander>(() =>
               {
                   var commander = new AsciiCommander();
                   commander.AddSynchronousResponder(); // handles synchronous command execution
                   commander.AddResponder(this.container.GetInstance<TranspondersMonitor>()); // handles asynchronous transponder received
                   commander.AddResponder(this.container.GetInstance<BarcodeMonitor>()); // handles asynchronous barcodes received

                   return commander;
               });

            // The service that performs the read
            this.container.Register<TagReader>();

            // The service that performs the write
            this.container.Register<TagWriter>();

            // the asynchronous responder for transponders that reports as IMontiorsTransponders        
            this.container.Register(() => { return TranspondersMonitor.ForInventory(); });
            this.container.Register<IMonitorTransponders>(() => { return this.container.GetInstance<TranspondersMonitor>(); });

            // the asynchronous responder for barcodes that response as IMonitorBarcode
            this.container.Register<BarcodeMonitor>();
            this.container.Register<IMonitorBarcode>(() => { return this.container.GetInstance<BarcodeMonitor>(); });

            this.container.Register<BarcodeInventory>();
            this.container.Register<InventoryStatistics>();
            this.container.Register<TransponderInventory>();

            // Converts absolute RSSI to relative scale 0.0 to 1.0
            this.container.Register<ISignalNormalization>(() => { return new SignalNormalization(); });

            this.container.Register<ReaderInformation>();
            this.container.Register<ReaderInformationProvider>(true);

            // Register the Inventory configuration instances
            this.container.Register<InventoryConfiguration>(true);
            this.container.Register<IInventoryConfigurator>(() =>
            {
                return new InventoryConfigurator(
                    this.container.GetInstance<IAsciiCommander>(),
                    this.container.GetInstance<InventoryConfiguration>());
            });
          
        }

        /// <summary>
        /// Gets the view model for the inventory
        /// </summary>
        public InventoryViewModel InventoryView
        {
            get
            {
                return this.container.GetInstance<InventoryViewModel>();
            }
        }
        public HomeViewModel HomeView
        {
            get
            {
                return this.container.GetInstance<HomeViewModel>();
            }
       
        }
        /// Gets the view model for the GuestMaster
        /// </summary>
        //public GuestMasterViewModel GuestMasterView
        //{
        //    get
        //    {
        //        return this.container.GetInstance<GuestMasterViewModel>();
        //    }
        //}
        /// <summary>
        /// Gets the view model for the GuestListView
        /// </summary>
        //public GuestListViewModel GuestListView => this.container.GetInstance<GuestListViewModel>();
        //public CheckInOutViewModel CheckInOuttView => this.container.GetInstance<CheckInOutViewModel>();

        //public AttendeeProfileViewModel AttendeeProfileView => this.container.GetInstance<AttendeeProfileViewModel>();

        //public GuestListDetailViewModel GuestListDetailView => this.container.GetInstance<GuestListDetailViewModel>();
        /// <summary>
        /// Gets the view model for read/write
        /// </summary>
        public ReadWriteViewModel ChangeRWView
        {
            get
            {
                return this.container.GetInstance<ReadWriteViewModel>();
            }
        }
        //public GuestListViewModel GuestListView
        //{
        //    get
        //    {
        //        return this.container.GetInstance<GuestListViewModel>();
        //    }
        //}
        //public CheckInOutViewModel CheckInOut
        //{
        //    get
        //    {
        //        Debug.WriteLine("CheckInOut");
        //        return ServiceLocator.Current.GetInstance<CheckInOutViewModel>();
        //    }
        //}
        

    }
}
