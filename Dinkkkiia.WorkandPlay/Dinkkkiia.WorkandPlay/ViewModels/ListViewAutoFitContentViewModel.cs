﻿using System.Collections.ObjectModel;
using Dinkkkiia.WorkandPlay.Models;
using GalaSoft.MvvmLight;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    public class ListViewAutoFitContentViewModel : ViewModelBase
    {
        #region Fields

        private ListViewBookInfo selectedItem;
        private ObservableCollection<ListViewBookInfo> bookInfo;

        #endregion

        #region Constructor

        public ListViewAutoFitContentViewModel()
        {
            GenerateSource();
        }

        #endregion

        #region Properties

        public ListViewBookInfo SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; }
        }


        public ObservableCollection<ListViewBookInfo> BookInfo
        {
            get { return bookInfo; }
            set { this.bookInfo = value; }
        }

        #endregion

        #region Generate Source

        private void GenerateSource()
        {
            ListViewBookInfoRepository bookInfoRepository = new ListViewBookInfoRepository();
            bookInfo = bookInfoRepository.GetBookInfo();
            SelectedItem = bookInfo[2];
        }

        #endregion

    }

}
