﻿//-----------------------------------------------------------------------
// <copyright file="LicenceViewModel.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2013 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Models;
using Dinkkkiia.WorkandPlay.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    /// <summary>
    /// ViewModel to perform an Inventory
    /// </summary>
    public class LicenceViewModel
        : ViewModelBase
    {
        /// <summary>
        /// Backing field for IsAuthorised
        /// </summary>
        private bool isAuthorised;

        /// <summary>
        /// Backing field for IsAuthorisedReadersOnlyEnabled
        /// </summary>
        private bool isAuthorisedReadersOnlyEnabled;

        /// <summary>
        /// Provides the interface to execute ASCII commands
        /// </summary>
        private IAsciiCommander commander;

        /// <summary>
        /// Gets or sets the settings for the authorisation
        /// </summary>
        private ISettings settings;

        /// <summary>
        /// The dispatcher used to invoke the messages to the user interface thread
        /// </summary>
        private IDispatcher dispatcher;

        /// <summary>
        /// Backing field for <see cref="IsBusy"/>
        /// </summary>
        private bool isBusy;

        /// <summary>
        /// Receive application notifications when a reader connects or disconnects
        /// </summary>
        private ReaderChangeNotification readerChangeNotification;

        /// <summary>
        /// Controls reporting of asynchronous barcodes in the application. IsEnabled property is set when authorised
        /// </summary>
        private IMonitorBarcode barcodes;

        /// <summary>
        /// Controls reporting of asynchronous transponders in the application. IsEnabled property is set when authorised
        /// </summary>
        private IMonitorTransponders transponders;

        /// <summary>
        /// Initializes a new instance of the LicenceViewModel class
        /// </summary>
        /// <param name="dispatcher">Used to invoke Messages on the user interface thread</param>
        /// <param name="commander">Used to send commands to the reader</param>
        /// <param name="settings">The settings to use for authorisation</param>
        /// <param name="barcodes">Used to control whether barcode scans are enabled</param>
        /// <param name="transponders">Used to control whether transponder scans are enabled</param>
        public LicenceViewModel(IDispatcher dispatcher, IAsciiCommander commander, ISettings settings, IMonitorBarcode barcodes, IMonitorTransponders transponders)
        {
            if (commander == null)
            {
                throw new ArgumentNullException("commander");
            }

            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            this.dispatcher = dispatcher;

            this.barcodes = barcodes;
            this.barcodes.IsEnabled = this.IsReaderAuthrisedOrAuthorisationOff;

            this.transponders = transponders;
            this.transponders.IsEnabled = this.IsReaderAuthrisedOrAuthorisationOff;

            // when the setting change re-evaluate the authorisation
            this.settings = settings;
            this.settings.PropertyChanged += (sender, e) => { this.VerifyAuthorisation(); };

            this.commander = commander;

            this.Messages = new ObservableCollection<string>();

            this.BarcodeCommand = new RelayCommand(this.ExecuteBarcode, this.CanExecuteIfAuthorisedOrAuthorisationOff);
            this.InventoryCommand = new RelayCommand(this.ExecuteInventory, this.CanExecute);
            this.AuthoriseCommand = new RelayCommand(this.ExecuteAuthoriseReader, this.CanExecute);
            this.DeauthoriseCommand = new RelayCommand(this.ExecuteDeauthoriseReader, this.CanExecute);

            this.readerChangeNotification = new ReaderChangeNotification(this.ReaderConnected, this.ReaderDisconnected);
            this.IsAuthorised = false;
        }

        /// <summary>
        /// Gets the messages to display to the user
        /// </summary>
        public ObservableCollection<string> Messages { get; private set; }

        /// <summary>
        /// Gets the command to scan a barcode
        /// </summary>
        public ICommand BarcodeCommand { get; private set; }

        /// <summary>
        /// Gets the command to perform an inventory
        /// </summary>
        public ICommand InventoryCommand { get; private set; }

        /// <summary>
        /// Gets the command to authorise the reader
        /// </summary>
        public ICommand AuthoriseCommand { get; private set; }

        /// <summary>
        /// Gets the command to deauthorise the reader
        /// </summary>
        public ICommand DeauthoriseCommand { get; private set; }

        /// <summary>
        /// Gets a text representation of the authorisation status
        /// </summary>
        public string AuthorisedStatus { get; private set; }        

        /// <summary>
        /// Gets a colour name to represent the authorisation status
        /// </summary>
        public string AuthorisedStatusColour { get; private set; }        

        /// <summary>
        /// Gets a value indicating whether the connected reader is authorised for use with this application
        /// </summary>
        public bool IsAuthorised
        {
            get
            {
                return this.isAuthorised;
            }

            private set
            {
                if (this.Set(ref this.isAuthorised, value))
                {
                    this.UpdateCanExecute();

                    // enable or disable reception of aync based upon authorisation
                    this.transponders.IsEnabled = this.barcodes.IsEnabled = this.IsAuthorised;
                }

                if (!this.commander.CanExecuteCommand())
                {
                    this.AuthorisedStatus = "No reader connected";
                    this.AuthorisedStatusColour = "Blue";
                }
                else if (this.IsAuthorised)
                {
                    this.AuthorisedStatus = "Authorised";
                    this.AuthorisedStatusColour = "Green";
                }
                else
                {
                    this.AuthorisedStatus = "Not Authorised";
                    this.AuthorisedStatusColour = "Red";
                }

                this.OnMessage("Reader is authorised: " + (this.isAuthorised ? "YES" : "NO"));
                this.RaisePropertyChanged("AuthorisedStatus");
                this.RaisePropertyChanged("AuthorisedStatusColour");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this reader will work with authorised readers only
        /// </summary>
        public bool IsAuthorisedReadersOnlyEnabled
        {
            get
            {
                return this.isAuthorisedReadersOnlyEnabled;
            }

            set
            {
                if (this.Set(ref this.isAuthorisedReadersOnlyEnabled, value))
                {
                    this.UpdateCanExecute();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the application should operate with the reader either because the reader is authorised or authorisation is turned off
        /// </summary>
        private bool IsReaderAuthrisedOrAuthorisationOff
        {
            get
            {
                return !this.IsAuthorisedReadersOnlyEnabled || this.IsAuthorised;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether an action is being performed
        /// </summary>
        private bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                if (this.Set(ref this.isBusy, value))
                {
                    this.UpdateCanExecute();
                }
            }
        }

        /// <summary>
        /// Identifies the connected reader and uses the identity to program an appropriate licence key
        /// </summary>
        public void ExecuteAuthoriseReader()
        {
            try
            {
                this.IsBusy = true;

                var version = this.commander.Execute(new VersionInformationCommand());
                bool authorised;

                if (LicenceKey.IsCommandSupported(version))
                {
                    LicenceKeyCommand licenceCommand;

                    var licence = LicenceKey.Compute(version.SerialNumber, this.settings.CompanyName, this.settings.Secret);
                    licenceCommand = new LicenceKeyCommand() { DeleteKey = Deletion.Yes, LicenceKey = licence };
                    this.commander.Execute(licenceCommand);
                    if (licenceCommand.Response.IsSuccessful)
                    {
                        this.OnMessage("Licence key programmed");
                        authorised = true;
                    }
                    else
                    {
                        this.OnMessage("Failed to program licence key");
                        this.OnMessage(licenceCommand.Response.Messages.First());
                        authorised = false;
                    }
                }
                else
                {
                    this.OnMessage("LicenceKey command not supported in this firmware");
                    this.OnMessage(version.AsciiProtocol);
                    authorised = false;
                }

                this.IsAuthorised = authorised;
            }
            catch (Exception ex)
            {
                this.HandleError(ex);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        /// <summary>
        /// Performs an barcode scan
        /// </summary>
        public void ExecuteBarcode()
        {
            if (this.IsReaderAuthrisedOrAuthorisationOff)
            {
                try
                {
                    this.IsBusy = true;
                    this.commander.Transmit(new BarcodeCommand());
                }
                catch (Exception ex)
                {
                    this.HandleError(ex);
                }
                finally
                {
                    this.IsBusy = false;
                }
            }
            else
            {
                this.OnMessage("Reader is not authorised to perform a barcode scan");
            }
        }

        /// <summary>
        /// Deletes the licence key from the connected reader
        /// </summary>
        public void ExecuteDeauthoriseReader()
        {
            try
            {
                this.IsBusy = true;
                LicenceKeyCommand licenceCommand;

                licenceCommand = new LicenceKeyCommand() { DeleteKey = Deletion.Yes };
                this.commander.Execute(licenceCommand);
                if (licenceCommand.Response.IsSuccessful)
                {
                    this.OnMessage("Licence key deleted");
                }
                else
                {
                    this.OnMessage("Failed to delete licence key");
                    this.OnMessage(licenceCommand.Response.Messages.First());
                }

                this.IsAuthorised = false;
            }
            catch (Exception ex)
            {
                this.HandleError(ex);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        /// <summary>
        /// Performs an inventory
        /// </summary>
        public void ExecuteInventory()
        {
            if (this.IsReaderAuthrisedOrAuthorisationOff)
            {
                try
                {
                    this.IsBusy = true;
                    this.commander.Transmit(new InventoryCommand());
                }
                catch (Exception ex)
                {
                    this.HandleError(ex);
                }
                finally
                {
                    this.IsBusy = false;
                }
            }
            else
            {
                this.OnMessage("Reader is not authorised to perform inventory");
            }
        }

        /// <summary>
        /// Check that the reader is authorised to operate with this hardware
        /// </summary>
        /// <remarks>
        /// This would normally be in the service but moved into the view model to provide diagnostic messages to the view
        /// </remarks>
        public void VerifyAuthorisation()
        {
            try
            {
                this.IsBusy = true;
                bool authorised;

                if (this.commander.CanExecuteCommand())
                {
                    var version = this.commander.Execute(new VersionInformationCommand());

                    if (LicenceKey.IsCommandSupported(version))
                    {
                        var licenceCommand = this.commander.Execute(new LicenceKeyCommand());

                        if (licenceCommand.Response.IsSuccessful)
                        {
                            this.OnMessage("Expected Licence: " + LicenceKey.Compute(version.SerialNumber, this.settings.CompanyName, this.settings.Secret));
                            this.OnMessage("Reader Licence: " + licenceCommand.LicenceKey);
                            authorised = LicenceKey.Verify(version.SerialNumber, this.settings.CompanyName, this.settings.Secret, licenceCommand.LicenceKey);
                        }
                        else
                        {
                            this.OnMessage("Failed to read licence key");
                            this.OnMessage(licenceCommand.Response.Messages.First());
                            authorised = false;
                        }
                    }
                    else
                    {
                        this.OnMessage("LicenceKey command not supported in this firmware");
                        this.OnMessage(version.AsciiProtocol);
                        authorised = false;
                    }
                }
                else
                {
                    authorised = false;
                }

                this.IsAuthorised = authorised;
            }
            catch (Exception ex)
            {
                this.HandleError(ex);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        /// <summary>
        /// Raises the Message event on the UI thread
        /// </summary>
        /// <param name="message">The message to notify the UI with</param>
        protected virtual void OnMessage(string message)
        {
            this.dispatcher.InvokeOnUserInterfaceThread(() =>
           {
               this.Messages.Add(message);
           });
        }

        /// <summary>
        /// Called by ReaderChangeNotification as the reader connects
        /// </summary>
        /// <param name="version">The version of the connected reader</param>
        private void ReaderConnected(VersionInformationCommand version)
        {
            this.UpdateCanExecute();
            this.VerifyAuthorisation();
        }

        /// <summary>
        /// Called by ReaderChangeNotification as the reader disconnects
        /// </summary>
        private void ReaderDisconnected()
        {
            this.UpdateCanExecute();
            this.VerifyAuthorisation();
        }

        /// <summary>
        /// Updates the various commands based on the current status
        /// </summary>
        private void UpdateCanExecute()
        {
            (this.BarcodeCommand as RelayCommand).RaiseCanExecuteChanged();
            (this.InventoryCommand as RelayCommand).RaiseCanExecuteChanged();
            (this.AuthoriseCommand as RelayCommand).RaiseCanExecuteChanged();
            (this.DeauthoriseCommand as RelayCommand).RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Returns a value indicating whether a command can execute. When a reader is connected and idle
        /// </summary>
        /// <returns>True when a reader is connected and idle</returns>
        private bool CanExecute()
        {
            return !this.IsBusy && this.commander.CanExecuteCommand();
        }

        /// <summary>
        /// Returns a value indicating whether a command can execute. When a reader is connected, idle and authorised or authorisation is not required
        /// </summary>
        /// <returns>True when a reader is connected, idle and authorised or authorisation is not required</returns>
        private bool CanExecuteIfAuthorisedOrAuthorisationOff()
        {
            return this.CanExecute() && this.IsReaderAuthrisedOrAuthorisationOff;
        }

        /// <summary>
        /// Display the error to the user
        /// </summary>
        /// <param name="ex">The exception to handle</param>
        private void HandleError(Exception ex)
        {
            this.OnMessage("ERROR");
            this.OnMessage(ex.Message);
        }
    }
}