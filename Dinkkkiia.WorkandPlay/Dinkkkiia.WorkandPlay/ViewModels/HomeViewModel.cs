﻿using System.Collections.ObjectModel;
using Dinkkkiia.WorkandPlay.Models;
using GalaSoft.MvvmLight;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    public class HomeViewModel
        : ViewModelBase
    {
        private OrderInfoRepository order;
        public HomeViewModel()
        {
            SetRowsToGenerate(50);
        }
        //ItemsSource
        private ObservableCollection<OrderInfo> ordersInfo;

        public ObservableCollection<OrderInfo> OrdersInfo
        {
            get { return ordersInfo; }
            set { this.ordersInfo = value; }
        }

        //ItemsSource Generator
        public void SetRowsToGenerate(int count)
        {
            order = new OrderInfoRepository();
           // ordersInfo = order.GetOrderDetails(count);
        }
    }
}
