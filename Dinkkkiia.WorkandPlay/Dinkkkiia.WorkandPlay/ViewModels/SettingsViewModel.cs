﻿//-----------------------------------------------------------------------
// <copyright file="SettingsViewModel.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Windows.Input;
using Dinkkkiia.WorkandPlay.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    /// <summary>
    /// View model to edit the settings
    /// </summary>
    public class SettingsViewModel
        : ViewModelBase
    {
        /// <summary>
        /// Local cache of settings
        /// </summary>
        private ISettings settings;

        /// <summary>
        /// Backing field for <see cref="CompanyName"/>
        /// </summary>
        private string companyName;

        /// <summary>
        /// Backing field for <see cref="Secret"/>
        /// </summary>
        private string secret;

        /// <summary>
        /// Backing field for CanExecuteApplyChanges
        /// </summary>
        //private bool canExecuteApplyChanges;

        /// <summary>
        /// Initializes a new instance of the SettingsViewModel class
        /// </summary>
        /// <param name="settings">The settings to edit</param>
        public SettingsViewModel(ISettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            this.settings = settings;
            this.settings.PropertyChanged += this.Settings_PropertyChanged;

            this.ApplyChangesCommand = new RelayCommand(this.ExecuteApplyChanges, this.CanExecuteApplyOrRevertChanges);
            this.RevertChangesCommand = new RelayCommand(
                () => { this.CompanyName = this.settings.CompanyName; this.Secret = this.settings.Secret; },
                this.CanExecuteApplyOrRevertChanges);

            this.CompanyName = this.settings.CompanyName;
            this.Secret = this.settings.Secret;
        }

        /// <summary>
        /// Gets the command to apply the changes to the company name and secret
        /// </summary>
        public ICommand ApplyChangesCommand { get; private set; }

        /// <summary>
        /// Gets the command to revert the changes to the company name and secret
        /// </summary>
        public ICommand RevertChangesCommand { get; private set; }

        /// <summary>
        /// Gets or sets the company name
        /// </summary>
        public string CompanyName 
        {
            get
            {
                return this.companyName;
            }
            
            set
            {
                if (this.Set(ref this.companyName, value, "CompanyName"))
                {
                    (this.ApplyChangesCommand as RelayCommand).RaiseCanExecuteChanged();
                    (this.RevertChangesCommand as RelayCommand).RaiseCanExecuteChanged();
                }
            } 
        }

        /// <summary>
        /// Gets or sets the secret
        /// </summary>
        public string Secret 
        { 
            get
            {
                return this.secret;
            }

            set
            {
                if (this.Set(ref this.secret, value, "Secret"))
                {
                    (this.ApplyChangesCommand as RelayCommand).RaiseCanExecuteChanged();
                    (this.RevertChangesCommand as RelayCommand).RaiseCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// Returns a value indicating whether ApplyChanges can be called
        /// </summary>
        /// <returns>True if the displayed values are different to the application settings</returns>
        private bool CanExecuteApplyOrRevertChanges()
        {
            return this.CompanyName != this.settings.CompanyName ||
                this.Secret != this.settings.Secret;
        }

        /// <summary>
        /// Updates the settings with the new values
        /// </summary>
        private void ExecuteApplyChanges()
        {
            this.settings.CompanyName = this.CompanyName;
            this.settings.Secret = this.Secret;
        }        

        /// <summary>
        /// Updates the local settings to match the stored settings
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">Data provided for the event</param>
        private void Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ("CompanyName".Equals(e.PropertyName))
            {
                this.CompanyName = this.settings.CompanyName;
            }
            else if ("Secret".Equals(e.PropertyName))
            {
                this.Secret = this.settings.Secret;
            }

            (this.ApplyChangesCommand as RelayCommand).RaiseCanExecuteChanged();
            (this.RevertChangesCommand as RelayCommand).RaiseCanExecuteChanged();
        }        
    }
}
