﻿//-----------------------------------------------------------------------
// <copyright file="ReadWriteViewModel.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    /// <summary>
    /// ViewModel for the ReadWriteView
    /// </summary>
    public class ReadWriteViewModel
        : ViewModelBase, ILifeCycle
    {
        /// <summary>
        /// notifies this instance as the reader connects and disconnects
        /// </summary>
        private ReaderChangeNotification readerChangeNotication;

        /// <summary>
        /// The instance used to read tag
        /// </summary>
        private TagReader tagReader;

        /// <summary>
        /// The instance used to write tag
        /// </summary>
        private TagWriter tagWriter;

        /// <summary>
        /// The list of messages displayed to the user
        /// </summary>
        private ObservableCollection<MessageItem> messages = new ObservableCollection<MessageItem>();

        /// <summary>
        /// Backing store for IsBusy property
        /// </summary>
        private bool isIdle;

        /// <summary>
        /// Backing store for the HexIdentifier property
        /// </summary>
        private string hexIdentifier = string.Empty;

        /// <summary>
        /// Backing store for the MemoryBank property
        /// </summary>
        private int memoryBank = 1;

        /// <summary>
        /// Backing store for the WordAddress property
        /// </summary>
        private int wordAddress = 2;

        /// <summary>
        /// Backing store for the WordCount property
        /// </summary>
        private int wordCount = 2;

        /// <summary>
        /// Backing store for the MinimumPower property
        /// </summary>
        private int minimumPower = 4;

        /// <summary>
        /// Backing store for the MaximumPower property
        /// </summary>
        private int maximumPower = 29;

        /// <summary>
        /// Backing store for the OutputPower property
        /// </summary>
        private int outputPower = 29;

        /// <summary>
        /// Backing store for the HexData property
        /// </summary>
        private string hexData;

        /// <summary>
        /// Used to invoke actions on the user interface thread
        /// </summary>
        private IDispatcher dispatcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWriteViewModel"/> class.
        /// </summary>
        /// <param name="tagReader">used to read a tag</param>
        /// <param name="tagWriter">used to write a tag</param>
        /// <param name="dispatcher">used to dispatch to the UI thread</param>
        public ReadWriteViewModel(TagReader tagReader, TagWriter tagWriter, IDispatcher dispatcher)
        {
            this.tagReader = tagReader;
            this.tagReader.ProgressUpdate += this.ReadWrite_ProgressUpdate;

            this.tagWriter = tagWriter;
            this.tagWriter.ProgressUpdate += this.ReadWrite_ProgressUpdate;

            this.dispatcher = dispatcher;

            this.ReadTagCommand = new RelayCommand(this.ExecuteReadTag);
            this.ClearCommand = new RelayCommand(this.ExecuteClear);
            this.WriteTagCommand = new RelayCommand(this.ExecuteWriteTag, this.CanExecuteWriteTag);

            this.readerChangeNotication = new ReaderChangeNotification(this.ReaderConnected, () => { });

            this.MinimumPower = 10;
            this.MaximumPower = 29;
            this.OutputPower = this.MaximumPower;

            this.IsIdle = true;
        }

        /// <summary>
        /// Gets the command to read the tag
        /// </summary>
        public ICommand ReadTagCommand { get; private set; }

        /// <summary>
        /// Gets the command to read the tag
        /// </summary>
        public ICommand ClearCommand { get; private set; }

        /// <summary>
        /// Gets the command to read the tag
        /// </summary>
        public ICommand WriteTagCommand { get; private set; }

        /// <summary>
        /// Gets or sets the new EPC filter
        /// </summary>
        public string HexIdentifier
        {
            get
            {
                return this.hexIdentifier;
            }

            set
            {
                this.Set(ref this.hexIdentifier, value);
            }
        }

        /// <summary>
        /// Gets or sets the memoryBank
        /// </summary>
        public int MemoryBank
        {
            get
            {
                return this.memoryBank;
            }

            set
            {
                this.Set(ref this.memoryBank, (int)value);
            }
        }

        /// <summary>
        /// Gets or sets the wordAddress
        /// </summary>
        public int WordAddress
        {
            get
            {
                return this.wordAddress;
            }

            set
            {
                this.Set(ref this.wordAddress, (int)value);
            }
        }

        /// <summary>
        /// Gets or sets the wordCount
        /// </summary>
        public int WordCount
        {
            get
            {
                return this.wordCount;
            }

            set
            {
                this.Set(ref this.wordCount, (int)value);
            }
        }

        /// <summary>
        /// Gets or sets the minimumPower
        /// </summary>
        public int MinimumPower
        {
            get
            {
                return this.minimumPower;
            }

            set
            {
                this.Set(ref this.minimumPower, (int)value);
                RaisePropertyChanged("MinimumPower");
            }
        }

        /// <summary>
        /// Gets or sets the maximumPower
        /// </summary>
        public int MaximumPower
        {
            get
            {
                return this.maximumPower;
            }

            set
            {
                this.Set(ref this.maximumPower, (int)value);
                RaisePropertyChanged("MaximumPower");
            }
        }

        /// <summary>
        /// Gets or sets the outputPower
        /// </summary>
        public int OutputPower
        {
            get
            {
                return this.outputPower;
            }

            set
            {
                this.Set(ref this.outputPower, (int)value);
                RaisePropertyChanged("OutputPower");
            }
        }

        /// <summary>
        /// Gets or sets the data
        /// </summary>
        public string HexData
        {
            get
            {
                return this.hexData;
            }

            set
            {
                this.Set(ref this.hexData, value);
                (this.WriteTagCommand as RelayCommand).RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Gets or sets the progress messages
        /// </summary>
        public ObservableCollection<MessageItem> ProgressMessages
        {
            get
            {
                return this.messages;
            }

            set
            {
                this.Set(ref this.messages, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a long running task is not executing
        /// </summary>
        public bool IsIdle
        {
            get
            {
                return this.isIdle;
            }

            set
            {
                this.Set(ref this.isIdle, value);
            }
        }

        /// <summary>
        /// Lifecycle event when view is hidden
        /// </summary>
        public void Hidden()
        {
        }

        /// <summary>
        /// Lifecycle event when view is shown
        /// </summary>
        public void Shown()
        {
        }

        /// <summary>
        /// Performs the <see cref="ReadTagCommand"/>
        /// </summary>
        private async void ExecuteReadTag()
        {
            this.IsIdle = false;
            this.messages.Clear();

            // Execute the potentially long running task off the UI thread
            await Task.Run(() =>
            {
                try
                {
                if (this.tagReader.ReadTags(this.HexIdentifier.ToLower(), this.MemoryBank, this.WordAddress, this.WordCount, this.OutputPower))
                    {
                        this.AppendMessage("Done.");
                    }
                }
                catch (Exception ex)
                {
                    this.AppendMessage(ex.Message);
                }
            });

            this.IsIdle = true;
        }

        /// <summary>
        /// Performs the <see cref="ClearCommand"/>
        /// </summary>
        private void ExecuteClear()
        {
            this.messages.Clear();
            this.AppendMessage("Cleared...");
        }

        /// <summary>
        /// Determines if the WriteTag command can execute
        /// </summary>
        /// <returns>true when the EPC can be changed</returns>
        private bool CanExecuteWriteTag()
        {
            return HexValidator.IsValidWordAlignedHex(this.HexData);
        }

        /// <summary>
        /// Performs the <see cref="WriteTagCommand"/>
        /// </summary>
        private async void ExecuteWriteTag()
        {
            this.IsIdle = false;
            this.messages.Clear();

            // Execute the potentially long running task off the UI thread
            await Task.Run(() =>
            {
                try
                {
                    if (this.tagWriter.WriteTags(this.HexIdentifier.ToLower(), this.MemoryBank, this.WordAddress, this.WordCount, this.HexData, this.OutputPower))
                    {
                        this.AppendMessage("Done.");
                    }
                }
                catch (Exception ex)
                {
                    this.AppendMessage(ex.Message);
                }
            });

            this.IsIdle = true;
            (this.ReadTagCommand as RelayCommand).RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Respond to progress messages 
        /// </summary>
        /// <param name="sender">the source of the event</param>
        /// <param name="e">the progress update arguments</param>
        private void ReadWrite_ProgressUpdate(object sender, ProgressEventArgs e)
        {
            this.AppendMessage(e.ProgressMessage);
        }

        /// <summary>
        /// Handles change in reader state
        /// </summary>
        /// <param name="version">an executed version command for the connected reader</param>
        private void ReaderConnected(VersionInformationCommand version)
        {
            // Set the correct slider power limits for the currently connected reader
            this.MaximumPower = LibraryConfiguration.Current.MaximumOutputPower;
            this.MinimumPower = LibraryConfiguration.Current.MinimumOutputPower;

            // Set the reader to the maximum allowed power
            this.OutputPower = this.MaximumPower;
        }

        /// <summary>
        /// Appends the given string to the end of the message list
        /// </summary>
        /// <param name="message">The message to append</param>
        private void AppendMessage(string message)
        {
            this.dispatcher.InvokeOnUserInterfaceThread(() =>
            {
                while (this.messages.Count > 50)
                {
                    this.messages.RemoveAt(0);
                }

                this.messages.Add(new MessageItem(message));
            });
        }

        /// <summary>
        /// Provides a bind-able item for the message ListView
        /// </summary>
        public class MessageItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="MessageItem" /> class.
            /// </summary>
            /// <param name="message">the item's message</param>
            public MessageItem(string message)
            {
                this.MessageLine = message;
            }

            /// <summary>
            /// Gets or sets the items single-line message
            /// </summary>
            public string MessageLine { get; set; }
        }
    }
}
