﻿//-----------------------------------------------------------------------
// <copyright file="InventoryViewModel.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Windows.Input;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Models;
using Dinkkkiia.WorkandPlay.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    /// <summary>
    /// ViewModel for the inventory view
    /// </summary>
    public class InventoryViewModel
        : ViewModelBase
    {
        /// <summary>
        /// Used to navigate to other view models
        /// </summary>
        private INavigationService navigator;
        /// <summary>
        /// Maintains the list of unique barcodes
        /// </summary>
        private BarcodeInventory barcodes;

        /// <summary>
        /// Maintains the list of unique transponders and statistics
        /// </summary>
        private TransponderInventory transponders;

        /// <summary>
        /// The required configuration for inventory
        /// </summary>
        private InventoryConfiguration configuration;

        /// <summary>
        /// Updates the reader's configuration for inventory
        /// </summary>
        private IInventoryConfigurator configurator;

        /// <summary>
        /// Backing field for <see cref="IsReaderConfiguring"/>
        /// </summary>
        private bool isReaderConfiguring;

        /// <summary>
        /// Backing field for <see cref="IsRssiRequested"/>
        /// </summary>
        private bool isRssiRequested;

        /// <summary>
        /// Backing field for <see cref="IsConfigurationModified"/>
        /// </summary>
        private bool isConfigurationModified;

        /// <summary>
        /// Backing field for <see cref="OutputPower"/>
        /// </summary>
        private double outputPower;

        /// <summary>
        /// Backing field for <see cref="MaximumPower"/>
        /// </summary>
        private int maximumPower;

        /// <summary>
        /// Backing field for <see cref="MinimumPower"/>
        /// </summary>
        private int minimumPower;

        /// <summary>
        /// notifies this instance as the reader connects and disconnects
        /// </summary>
        private ReaderChangeNotification readerChangeNotication;

        /// <summary>
        /// Initializes a new instance of the InventoryViewModel class
        /// </summary>
        /// <param name="statistics">The statistics to display</param>
        /// <param name="transponders">The unique transponders list</param>
        /// <param name="barcodes">The unique barcodes list</param>
        /// <param name="configurator">Used to update the reader configuration</param>
        /// <param name="configuration">The inventory configuration to apply</param>
        public InventoryViewModel(
            INavigationService navigator,
            InventoryStatistics statistics,
            TransponderInventory transponders,
            BarcodeInventory barcodes,
            IInventoryConfigurator configurator,
            InventoryConfiguration configuration)
        {
            this.navigator = navigator;
            this.barcodes = barcodes;
            this.Barcodes = this.barcodes.Identifiers;

            this.Statistics = statistics;

            this.transponders = transponders;
            this.Transponders = this.transponders.Identifiers;

           
            this.ClearCommand = new RelayCommand(this.ExecuteClear);
           this.AddCommand = new RelayCommand(this.GoToReadWrite);

            this.UpdateCommand = new RelayCommand(this.ExecuteUpdate, this.CanExecuteUpdate);

            this.configurator = configurator;
            this.configuration = configuration;

            this.IsRssiRequested = false;
            this.MaximumPower = 24;
            this.MinimumPower = 12;

            this.IsReaderConfiguring = false;

            this.readerChangeNotication = new ReaderChangeNotification(this.ReaderConnected, () => { });
        }

       
        private void GoToReadWrite()
        {

            navigator.NavigateTo("ReadWritePage");
        }

        /// <summary>
        /// Gets the unique barcodes scanned
        /// </summary>
        public ObservableCollection<IdentifiedItem> Barcodes { get; private set; }

        /// <summary>
        /// Gets the statistics for the RFID inventories performed
        /// </summary>
        public InventoryStatistics Statistics { get; private set; }

        /// <summary>
        /// Gets the unique transponders scanned
        /// </summary>
        public ObservableCollection<IdentifiedItem> Transponders { get; private set; }

        /// <summary>
        /// Gets the command to clear the barcodes, transponders and statistics
        /// </summary>
        public ICommand ClearCommand { get; private set; }
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the command to apply the inventory configuration to the connected reader
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the RSSI should be reported for transponders
        /// </summary>
        public bool IsRssiRequested
        {
            get
            {
                return this.isRssiRequested;
            }

            set
            {
                this.Set(ref this.isRssiRequested, value);
                this.configuration.IsRssiRequested = value;
                this.IsConfigurationModified = true;
            }
        }

        /// <summary>
        /// Gets or sets the RFID output power
        /// </summary>
        public double OutputPower
        {
            get
            {
                return this.outputPower;
            }

            set
            {
                this.Set(ref this.outputPower, value);
                this.configuration.OutputPower = (int)value;
                this.IsConfigurationModified = true;
            }
        }

        /// <summary>
        /// Gets or sets the maximum output power of the connected reader
        /// </summary>
        public double MaximumPower
        {
            get
            {
                return this.maximumPower;
            }

            set
            {
                this.Set(ref this.maximumPower, (int)value);
            }
        }

        /// <summary>
        /// Gets or sets the minimum output power of the connected reader
        /// </summary>
        public double MinimumPower
        {
            get
            {
                return this.minimumPower;
            }

            set
            {
                this.Set(ref this.minimumPower, (int)value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the inventory configuration is dirty
        /// </summary>
        private bool IsConfigurationModified
        {
            get
            {
                return this.isConfigurationModified;
            }

            set
            {
                if (this.Set(ref this.isConfigurationModified, value))
                {
                    (this.UpdateCommand as RelayCommand).RaiseCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the reader is currently being updated
        /// </summary>
        private bool IsReaderConfiguring
        {
            get
            {
                return this.isReaderConfiguring;
            }

            set
            {
                this.Set(ref this.isReaderConfiguring, value);
                (this.UpdateCommand as RelayCommand).RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Performs the <see cref="ClearCommand"/>
        /// </summary>
        private void ExecuteClear()
        {
            this.barcodes.Clear();
            this.Statistics.Clear();
            this.transponders.Clear();
        }

        /// <summary>
        /// Returns a value indicating whether the <see cref="UpdateCommand"/> can execute
        /// </summary>
        /// <returns>True if the configuration is changed and not already applying the change</returns>
        private bool CanExecuteUpdate()
        {
            return !this.isReaderConfiguring && this.isConfigurationModified;
        }

        /// <summary>
        /// Performs the <see cref="ClearCommand"/>
        /// </summary>
        private async void ExecuteUpdate()
        {
            this.IsReaderConfiguring = true;

            await this.configurator.ConfigureAsync();

            this.IsReaderConfiguring = false;
            this.IsConfigurationModified = false;
        }

        /// <summary>
        /// Gets the key for the <see cref="INavigationService"/> to navigate to the view for this ViewModel
        /// </summary>
        public static string NavigationKey
        {
            get
            {
                return "ReadWritePage";
            }
        }

        /// <summary>
        /// Called when a new reader is connected. Update the displayed parameters
        /// </summary>
        /// <param name="version">The version of the new reader</param>
        private void ReaderConnected(VersionInformationCommand version)
        {
            // Set the correct slider power limits for the currently connected reader
            this.MaximumPower = LibraryConfiguration.Current.MaximumOutputPower;
            this.MinimumPower = LibraryConfiguration.Current.MinimumOutputPower;

            // Set the reader to the maximum allowed power
            this.OutputPower = this.MaximumPower;
            this.ExecuteUpdate();
        }
    }
}
