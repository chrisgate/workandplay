﻿//-----------------------------------------------------------------------
// <copyright file="ChangeEpcViewModel.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.ViewModels
{
    /// <summary>
    /// ViewModel for the ChangeEpcView
    /// </summary>
    public class ChangeEpcViewModel
        : ViewModelBase, ILifeCycle
    {
        /// <summary>
        /// notifies this instance as the reader connects and disconnects
        /// </summary>
        private ReaderChangeNotification readerChangeNotication;

        /// <summary>
        /// The instance used to change the EPC of the tag
        /// </summary>
        private ProximityEpcChanger epcChanger;

        /// <summary>
        /// The list of messages displayed to the user
        /// </summary>
        private ObservableCollection<MessageItem> messages = new ObservableCollection<MessageItem>();

        /// <summary>
        /// Backing store for IsBusy property
        /// </summary>
        private bool isIdle;
        
        /// <summary>
        /// Backing store for the HexIdentifier property
        /// </summary>
        private string hexIdentifier;

        /// <summary>
        /// Used to invoke actions on the user interface thread
        /// </summary>
        private IDispatcher dispatcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangeEpcViewModel"/> class.
        /// </summary>
        /// <param name="epcChanger">used to change the EPC of a tag</param>
        /// <param name="dispatcher">used to dispatch to the UI thread</param>
        public ChangeEpcViewModel(ProximityEpcChanger epcChanger, IDispatcher dispatcher)
        {
            this.epcChanger = epcChanger;
            this.epcChanger.ProgressUpdate += this.EpcChanger_ProgressUpdate;

            this.dispatcher = dispatcher;

            this.ChangeEpcCommand = new RelayCommand(this.ExecuteChangeEpc, this.CanExecuteChangeEpc);

            this.readerChangeNotication = new ReaderChangeNotification(this.ReaderConnected, () => { });

            this.IsIdle = true;
        }

        /// <summary>
        /// Gets the command to change the epc
        /// </summary>
        public ICommand ChangeEpcCommand { get; private set; }

        /// <summary>
        /// Gets or sets the new EPC identifier
        /// </summary>
        public string HexIdentifier
        {
            get
            {
                return this.hexIdentifier;
            }

            set
            {
                this.Set(ref this.hexIdentifier, value);
                (this.ChangeEpcCommand as RelayCommand).RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<MessageItem> ProgressMessages
        {
            get
            {
                return this.messages;
            }

            set
            {
                this.Set(ref this.messages, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a long running task is not executing
        /// </summary>
        public bool IsIdle
        {
            get
            {
                return this.isIdle;
            }

            set
            {
                this.Set(ref this.isIdle, value);
            }
        }

        /// <summary>
        /// Lifecycle event when view is hidden
        /// </summary>
        public void Hidden()
        {
        }

        /// <summary>
        /// Lifecycle event when view is shown
        /// </summary>
        public void Shown()
        {
        }

        /// <summary>
        /// Determines if the ChangeEpc command can execute
        /// </summary>
        /// <returns>true when the EPC can be changed</returns>
        private bool CanExecuteChangeEpc()
        {
            return HexValidator.IsValidWordAlignedHex(this.HexIdentifier);
        }

        /// <summary>
        /// Performs the <see cref="ChangeEpcCommand"/>
        /// </summary>
        private async void ExecuteChangeEpc()
        {
            this.IsIdle = false;
            this.messages.Clear();

            // Execute the potentially long running task off the UI thread
            await Task.Run(() =>
            {
                try
                {
                    if (this.epcChanger.ChangeEpcTo(this.HexIdentifier.ToLower()))
                    {
                        this.AppendMessage("Done.");
                    }
                }
                catch (Exception ex)
                {
                    this.AppendMessage(ex.Message);
                }
            });

            this.IsIdle = true;
            (this.ChangeEpcCommand as RelayCommand).RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Respond to progress messages from the EPC Changer
        /// </summary>
        /// <param name="sender">the source of the event</param>
        /// <param name="e">the progress update arguments</param>
        private void EpcChanger_ProgressUpdate(object sender, ProgressEventArgs e)
        {
            this.AppendMessage(e.ProgressMessage);
        }

        /// <summary>
        /// Handles change in reader state
        /// </summary>
        /// <param name="version">an executed version command for the connected reader</param>
        private void ReaderConnected(VersionInformationCommand version)
        {
        }

        /// <summary>
        /// Appends the given string to the end of the message list
        /// </summary>
        /// <param name="message">The message to append</param>
        private void AppendMessage(string message)
        {
            this.dispatcher.InvokeOnUserInterfaceThread(() =>
            {
                while (this.messages.Count > 50)
                {
                    this.messages.RemoveAt(0);
                }

                this.messages.Add(new MessageItem(message));
            });
        }

        /// <summary>
        /// Provides a bind-able item for the message ListView
        /// </summary>
        public class MessageItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="MessageItem" /> class.
            /// </summary>
            /// <param name="message">the item's message</param>
            public MessageItem(string message)
            {
                this.MessageLine = message;
            }

            /// <summary>
            /// Gets or sets the items single-line message
            /// </summary>
            public string MessageLine { get; set; }
        }
    }
}
