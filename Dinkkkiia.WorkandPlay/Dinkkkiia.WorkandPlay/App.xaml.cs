﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Ioc;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Common.Views;
using Dinkkkiia.WorkandPlay.Startup;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Dinkkkiia.WorkandPlay
{
    public partial class App : Application
    {
        #region Fields


        private static ViewModelLocator locator;

        #endregion

        public App()
        {
            InitializeComponent();

            // ensure we have setup the container before launching the views
            var locator = App.Locator;

            var mainPage = new Views.AppShell();

            // Init Navigation Service
            ((NavigationService)SimpleIoc.Default.GetInstance<INavigationService>()).Initialize((NavigationPage)mainPage.Detail);
            MainPage = mainPage;

            // Init Dialog Service
            ((DialogService)SimpleIoc.Default.GetInstance<IDialogService>()).Initialize(MainPage);



            #region Test Code
            // create the platform specific (Xamarin Forms) navigation service
            //            var navigation = new NavigationService();
            //            var dialog = new DialogService();

            //            // register the posible navigations
            //            navigation.Configure(ConnectViewModel.NavigationKey, typeof(ConnectPage));
            //            //navigation.Configure(ViewModelLocator.AddCommentPageKey, typeof(AddCommentPage));
            //            //navigation.Configure(App.ShowCommentsPageKey, typeof(ShowCommentsPage));



            // tell the service locator to provide this as the INavigationService
            //            if (!SimpleIoc.Default.IsRegistered<INavigationService>())
            //            {
            //                SimpleIoc.Default.Register<INavigationService>(() => navigation);
            //                SimpleIoc.Default.Register<IDispatcher>(() => new Dispatcher());
            //                SimpleIoc.Default.Register<IDialogService>(() => dialog);
            //            }

            #endregion

        }

        #region Properties

        public static ViewModelLocator Locator
        {
            get
            {
                if (locator == null)
                {
                    // create the platform specific (Xamarin Forms) navigation service
                    var navigation = new NavigationService();

                    // register the posible navigations
                    navigation.Configure(ConnectViewModel.NavigationKey, typeof(ConnectPage));

                    // tell the service locator to provide this as the INavigationService
                    if (!SimpleIoc.Default.IsRegistered<INavigationService>())
                    {
                        SimpleIoc.Default.Register<INavigationService>(() => navigation);
                        SimpleIoc.Default.Register<IDispatcher>(() => { return new Dispatcher(); });
                    }

                    locator = new ViewModelLocator();
                }

                return locator;
            }
        }

        #endregion
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
