﻿using Dinkkkiia.WorkandPlay.Common.Models;
using Dinkkkiia.WorkandPlay.Common.Services;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Common.Views;
using Dinkkkiia.WorkandPlay.Models;
using Dinkkkiia.WorkandPlay.Services;
using Dinkkkiia.WorkandPlay.Views;
using GalaSoft.MvvmLight.Ioc;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Extensions;
using Settings = Dinkkkiia.WorkandPlay.Models.Settings;

//using CommonServiceLocator;

namespace Dinkkkiia.WorkandPlay.Startup
{
	/// <summary>
	/// Managing class for registering dependencies 
	/// Notes: You can change these registrations or add on to them in the native environments to inject native implementations
	/// You can also change these registrations in test projects to use test data with Dependency Injection
	/// </summary>
	public class IoCConfig
	{
		public IoCConfig()
		{
			//ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
		}

		public void RegisterSettings()
		{
			//SimpleIoc.Default.Register<IClientSettings, ClientSettings>();
		}

		public void RegisterRepositories()
		{
            // TODO:
		}

		public void RegisterServices()
		{
		    SimpleIoc.Default.Register<IDispatcher>(() => new Dispatcher());

            SimpleIoc.Default.Register<IDialogService>(() => new DialogService());
		}

		/// <summary>
		/// Registers the view models.
		/// </summary>
		public void RegisterViewModels()
		{
		    SimpleIoc.Default.Register<IReaderConnectionManager>(() => new ReaderConnectionManager());

		    SimpleIoc.Default.Register<ConnectViewModel>();
		    SimpleIoc.Default.Register<ReaderViewModel>();
            SimpleIoc.Default.Register<IPeopleService, PeopleService>();
			SimpleIoc.Default.Register<ViewModels.HomePageViewModel>();
			SimpleIoc.Default.Register<ViewModels.MenuPageViewModel>();
			SimpleIoc.Default.Register<ViewModels.SamplePageViewModel>();
			SimpleIoc.Default.Register<ViewModels.SettingsPageViewModel>();
		    SimpleIoc.Default.Register<ViewModels.ChangeEpcViewModel>();
		    SimpleIoc.Default.Register<ViewModels.InventoryViewModel>();
		    SimpleIoc.Default.Register<ViewModels.SportGroundViewModel>();
		    SimpleIoc.Default.Register<ViewModels.LicenceViewModel>();
		    SimpleIoc.Default.Register<ViewModels.ReadWriteViewModel>();
		    SimpleIoc.Default.Register<ViewModels.SettingsViewModel>();
		    SimpleIoc.Default.Register<ViewModels.ListViewAutoFitContentViewModel>();
		   

		    SimpleIoc.Default.Register<ISettings>(() => new Settings());
		    SimpleIoc.Default.Register<SwitchAsynchronousResponder>();
		    SimpleIoc.Default.Register<IAsciiCommander>(() =>
		    {
		        var commander = new AsciiCommander();
		        commander.AddSynchronousResponder(); // handles synchronous command execution
		        commander.AddResponder(SimpleIoc.Default.GetInstance<TranspondersMonitor>()); // handles asynchronous transponder received
		        commander.AddResponder(SimpleIoc.Default.GetInstance<BarcodeMonitor>()); // handles asynchronous barcodes received

		        return commander;
		    });
            // The service that performs the read
		    SimpleIoc.Default.Register<TagReader>();

            // The service that performs the write
		    SimpleIoc.Default.Register<TagWriter>();

            // the asynchronous responder for transponders that reports as IMontiorsTransponders        
		    SimpleIoc.Default.Register(() => { return TranspondersMonitor.ForInventory(); });
		    SimpleIoc.Default.Register<IMonitorTransponders>(() => SimpleIoc.Default.GetInstance<TranspondersMonitor>());

            // the asynchronous responder for barcodes that response as IMonitorBarcode
		    SimpleIoc.Default.Register<BarcodeMonitor>();
		    SimpleIoc.Default.Register<IMonitorBarcode>(() => { return SimpleIoc.Default.GetInstance<BarcodeMonitor>(); });

		    SimpleIoc.Default.Register<BarcodeInventory>();
		    SimpleIoc.Default.Register<InventoryStatistics>();
		    SimpleIoc.Default.Register<TransponderInventory>();

            // Converts absolute RSSI to relative scale 0.0 to 1.0
		    SimpleIoc.Default.Register<ISignalNormalization>(() => new SignalNormalization());

		    SimpleIoc.Default.Register<ReaderInformation>();
		    SimpleIoc.Default.Register<ReaderInformationProvider>(true);

            // Register the Inventory configuration instances
		    SimpleIoc.Default.Register<InventoryConfiguration>(true);
		    SimpleIoc.Default.Register<IInventoryConfigurator>(() => new InventoryConfigurator(
		        SimpleIoc.Default.GetInstance<IAsciiCommander>(),
		        SimpleIoc.Default.GetInstance<InventoryConfiguration>()));



            // TODO INIT: Register new ViewModels here
        }

        /// <summary>
        /// Handles navigation wire up
        /// </summary>
        public void RegisterNavigation()
		{
			var navigationService = new NavigationService();
			navigationService.Configure(nameof(HomePage), typeof(HomePage));
			navigationService.Configure(nameof(SportGround), typeof(SportGround));
//			navigationService.Configure(nameof(Views.WorkSheet), typeof(Views.WorkSheet));
			navigationService.Configure(nameof(MenuPage), typeof(MenuPage));
//            navigationService.Configure(nameof(Views.SamplePage), typeof(Views.SamplePage));
            navigationService.Configure(nameof(InventoryPage), typeof(InventoryPage));
            navigationService.Configure(nameof(ReadWritePage), typeof(ReadWritePage));
            navigationService.Configure(nameof(Views.Settings), typeof(Views.Settings));
//            navigationService.Configure(nameof(Views.SettingsPage), typeof(Views.SettingsPage));
            navigationService.Configure(nameof(ConnectPage), typeof(ConnectPage));
            navigationService.Configure(nameof(ReaderPage), typeof(ReaderPage));

			// TODO INIT: Register new Views here

			SimpleIoc.Default.Register<INavigationService>(() => navigationService);
		}
	}
}
