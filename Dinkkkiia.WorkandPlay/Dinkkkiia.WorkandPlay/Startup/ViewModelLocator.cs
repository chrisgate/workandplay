﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:OnionTemplate" x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  BindingContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using System.Diagnostics;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.ViewModels;
using GalaSoft.MvvmLight.Ioc;

namespace Dinkkkiia.WorkandPlay.Startup
{
	/// <summary>
	/// This class contains static references to all the view models in the
	/// application and provides an entry point for the bindings.
	/// </summary>
	public class ViewModelLocator
	{
		/// <summary>
		/// Initializes a new instance of the ViewModelLocator class.
		/// </summary>
		static ViewModelLocator()
		{
		    // Lets sets the Default singleton instance as the current ServiceLocator
//		    ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            Debug.WriteLine("ViewModelLocator");
            var iocConfig = new IoCConfig();
			iocConfig.RegisterSettings();
			iocConfig.RegisterRepositories();
			iocConfig.RegisterServices();
			iocConfig.RegisterViewModels();
			iocConfig.RegisterNavigation();
		}

		public static void Cleanup()
		{
			// TODO Clear the ViewModels
		}

        /* 
         * Define ViewModels
         */
        public HomePageViewModel HomePage => SimpleIoc.Default.GetInstance<HomePageViewModel>();
		public MenuPageViewModel MenuPage => SimpleIoc.Default.GetInstance<MenuPageViewModel>();
        public SamplePageViewModel SamplePage => SimpleIoc.Default.GetInstance<SamplePageViewModel>();
        public SettingsPageViewModel Settings => SimpleIoc.Default.GetInstance<SettingsPageViewModel>();
        public ChangeEpcViewModel ChangeEpcView => SimpleIoc.Default.GetInstance<ChangeEpcViewModel>();
        public InventoryViewModel Inventory => SimpleIoc.Default.GetInstance<InventoryViewModel>();
        public SportGroundViewModel SportGround => SimpleIoc.Default.GetInstance<SportGroundViewModel>();
        public LicenceViewModel LicenceView => SimpleIoc.Default.GetInstance<LicenceViewModel>();
        public ReadWriteViewModel ChangeRWView => SimpleIoc.Default.GetInstance<ReadWriteViewModel>();
        public SettingsViewModel SettingsView => SimpleIoc.Default.GetInstance<SettingsViewModel>();
        public ConnectViewModel ConnectView => SimpleIoc.Default.GetInstance<ConnectViewModel>();
        public ReaderViewModel ReaderView => SimpleIoc.Default.GetInstance<ReaderViewModel>();
        public ListViewAutoFitContentViewModel WorksheetView => SimpleIoc.Default.GetInstance<ListViewAutoFitContentViewModel>();
//
	    /// <summary>
	    /// Gets the view model for the inventory
	    /// </summary>
	    public InventoryViewModel InventoryView
	    {
	        get
	        {
                //return this.container.GetInstance<InventoryViewModel>();
                return SimpleIoc.Default.GetInstance<InventoryViewModel>();

            }
        }
//        public InventoryViewModel ChangeRWView
//        {
//	        get
//	        {
//                //return this.container.GetInstance<InventoryViewModel>();
//                return SimpleIoc.Default.GetInstance<ReadWriteViewModel>();
//
//            }
//        } 

//            public InventoryViewModel WorksheetView
//	    {
//	        get
//	        {
//	            //return this.container.GetInstance<InventoryViewModel>();
//	            return ServiceLocator.Current.GetInstance<ListViewAutoFitContentViewModel>();
//
//            }
//        }

        // TODO INIT: Add new ViewModels Here
    }
}
