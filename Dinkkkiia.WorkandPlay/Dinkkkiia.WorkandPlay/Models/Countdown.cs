﻿//
//  Countdown.cs
//  Created by Alexey Kinev on 11 Jan 2015.
//
//    Licensed under The MIT License (MIT)
//    http://opensource.org/licenses/MIT
//
//    Copyright (c) 2015 Alexey Kinev <alexey.rudy@gmail.com>
//

using System;
using System.Timers;
using GalaSoft.MvvmLight;

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <inheritdoc />
    /// <summary>
    /// Countdown timer with periodical ticks.
    /// </summary>
    public class Countdown : ViewModelBase
    {
        /// <summary>
        /// Gets the start date time.
        /// </summary>
        public DateTime StartDateTime { get; private set; }
        /// <summary>
        /// Gets the start date time.
        /// </summary>
        public DateTime SportStartDateTime { get; private set; } = DateTime.Now;

        /// <summary>
        /// Gets the remain time in seconds.
        /// </summary>
        public double RemainTime
        {
            get { return remainTime; }

            private set
            {
//                remainTime = value;
//                OnPropertyChanged();
                this.Set(ref this.remainTime, value);

            }
        }   
        /// <summary>
        /// Gets the remain time in seconds.
        /// </summary>
        public double TimeTicking
        {
            get { return remainTime; }

            private set
            {
//                remainTime = value;
//                OnPropertyChanged();
                this.Set(ref this.remainTime, value);

            }
        }  
        /// <summary>
        /// Gets the remain time in seconds.
        /// </summary>
        public double LapTime
        {
            get { return lapTime; }

            private set
            {
//                remainTime = value;
//                OnPropertyChanged();
                this.Set(ref this.lapTime, value);

            }
        }

        /// <summary>
        /// Occurs when completed.
        /// </summary>
        public event Action Completed;

        
        /// Occurs when ticked.
        /// </summary>
        public event Action Ticked;

        /// <summary>
        /// The timer.
        /// </summary>
        Timer timer;

        /// <summary>
        /// The remain time.
        /// </summary>
        double remainTime; 
        
        /// <summary>
        /// The remain time.
        /// </summary>
        double timeTicker; 
        /// <summary>
        /// The remain time.
        /// </summary>
        double lapTime;

        /// <summary>
        /// The remain time total.
        /// </summary> 
        
            /// <summary>
        /// The remain time total.
        /// </summary>
        double remainTimeTotal;
        
        /// <summary>
        /// The initial time.
        /// </summary>
        double initialTime = 0.0;

        /// <summary>
        /// Starts the updating participant time and period are specified in seconds.
        /// </summary>
        public void StartTiming(double period = 1.0)
        {
            if (timer != null)
            {
                StopUpdating();
            }

//            remainTimeTotal = total;
//            RemainTime = total;

            StartDateTime = DateTime.Now;

            timer = new Timer(period * 1000);
            timer = new Timer();
            //timer.Elapsed += (sender, e) => Tick();
            timer.Elapsed += (sender, e) => SportTick();
            timer.Enabled = true;
        } 
        /// <summary>
        /// Starts the updating with specified period, total time and period are specified in seconds.
        /// </summary>
        public void StartUpdating(double total, double period = 1.0)
        {
            if (timer != null)
            {
                StopUpdating();
            }

//            remainTimeTotal = total;
//            RemainTime = total;

            StartDateTime = DateTime.Now;

            timer = new Timer(period * 1000);
            timer = new Timer();
            //timer.Elapsed += (sender, e) => Tick();
            timer.Elapsed += (sender, e) => SportTick();
            timer.Enabled = true;
        }

        /// <summary>
        /// Stops the updating.
        /// </summary>
        public void StopUpdating()
        {
            RemainTime = 0;
            remainTimeTotal = 0;

            if (timer != null)
            {
                timer.Enabled = false;
                timer = null;
            }
        }

        /// <summary>
        /// Updates the time remain.
        /// </summary>
        public void Tick()
        {
            var delta = (DateTime.Now - StartDateTime).TotalSeconds;

            if (delta < remainTimeTotal)
            {
                RemainTime = remainTimeTotal - delta;

                var ticked = Ticked;
                if (ticked != null)
                {
                    ticked();
                }
            }
            else
            {
                RemainTime = 0;

                var completed = Completed;
                if (completed != null)
                {
                    completed();
                }
            }
        }
/// <summary>
        /// Updates the time remain.
        /// </summary>
        public void SportTick()
        {
            //var delta = (DateTime.Now - StartDateTime).TotalSeconds;
            var delta = (DateTime.Now - StartDateTime).TotalSeconds;

            if (delta >= 0.0)
            {
                TimeTicking = delta;

                var ticked = Ticked;
                if (ticked != null)
                {
                    ticked();
                }
            }
            else
            {
                TimeTicking = 0;

                var completed = Completed;
                if (completed != null)
                {
                    completed();
                }
            }
        }

        #region INotifyPropertyChanged implementation

//        /// <summary>
//        /// Occurs when property changed.
//        /// </summary>
//        public event PropertyChangedEventHandler PropertyChanged;
//
//        /// <summary>
//        /// Raises the property changed event.
//        /// </summary>
//        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
//        {
//            var handler = PropertyChanged;
//            if (handler != null)
//            {
//                handler(this, new PropertyChangedEventArgs(propertyName));
//            }
//        }

        #endregion
    }
}
