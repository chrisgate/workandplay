﻿//-----------------------------------------------------------------------
// <copyright file="Settings.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using GalaSoft.MvvmLight;

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <summary>
    /// Partial extension to the settings class to make it implement ISettings
    /// </summary>
    internal partial class Settings
        : ObservableObject, ISettings
    {
        /// <summary>
        /// Backing field for CompanyName
        /// </summary>
        private string companyName;

        /// <summary>
        /// Backing field for Secret
        /// </summary>
        private string secret;

        /// <summary>
        /// Initializes a new instance of the Settings class
        /// </summary>
        public Settings()
        {
            this.CompanyName = "Technology Solutions UK Ltd";
            this.Secret = "Setec Astronomy";
        }

        /// <summary>
        /// Gets or sets the company name
        /// </summary>
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }

            set
            {
                this.Set(ref this.companyName, value);
            }
        }

        /// <summary>
        /// Gets or sets the Secret
        /// </summary>
        public string Secret
        {
            get
            {
                return this.secret;
            }

            set
            {
                this.Set(ref this.secret, value);
            }
        }
    }
}
