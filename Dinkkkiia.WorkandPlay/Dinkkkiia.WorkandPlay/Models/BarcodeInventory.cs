﻿//-----------------------------------------------------------------------
// <copyright file="BarcodeInventory.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Services;

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <summary>
    /// View model for the inventory of transponders and barcodes
    /// </summary>
    public class BarcodeInventory
    {
        /// <summary>
        /// Used to invoke actions on the user interface thread
        /// </summary>
        private IDispatcher dispatcher;

        /// <summary>
        /// Initializes a new instance of the BarcodeInventory class
        /// </summary>
        /// <param name="barcode">Reports as barcodes are scanned</param>
        /// <param name="dispatcher">Used to update Identifiers on the user interface thread</param>
        public BarcodeInventory(IMonitorBarcode barcode, IDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;

            barcode.BarcodeScanned += (sender, e) => { this.AddBarcode(e.Barcode); };

            this.Identifiers = new ObservableCollection<IdentifiedItem>();
        }

        /// <summary>
        /// Gets the barcodes scanned
        /// </summary>
        public ObservableCollection<IdentifiedItem> Identifiers { get; private set; }

        /// <summary>
        /// Add a barcode to the list of barcodes
        /// </summary>
        /// <param name="identifier">The barcode identifier to add</param>
        public void AddBarcode(string identifier)
        {
            this.dispatcher.InvokeOnUserInterfaceThread(() =>
            {
                IdentifiedItem existing;

                existing = this.Identifiers
                    .Where(x => x.Identifier == identifier)
                    .FirstOrDefault();
                if (existing == null)
                {
                    this.Identifiers.Add(new IdentifiedItem(identifier, DateTime.Now));
                }
                else
                {
                    existing.SeenCount += 1;
                }
            });
        }

        /// <summary>
        /// Reset all of the statistics
        /// </summary>
        public void Clear()
        {
            this.Identifiers.Clear();
        }        
    }
}
