﻿using System.Collections.ObjectModel;

namespace Dinkkkiia.WorkandPlay.Models
{
    public class OrderInfoRepository
    {
        private ObservableCollection<OrderInfo> orderInfo;
        public ObservableCollection<OrderInfo> OrderInfoCollection
        {
            get { return orderInfo; }
            set { this.orderInfo = value; }
        }

        public OrderInfoRepository()
        {
            orderInfo = new ObservableCollection<OrderInfo>();
            this.GenerateOrders();
        }
//        public ObservableCollection<OrderInfo> GetOrderDetails(int count)
//        {
//            ObservableCollection<OrderInfo> orderDetails = new ObservableCollection<OrderInfo>();
//
//            for (int i = 10001; i <= count + 10000; i++)
//            {
//                var order = new OrderInfo()
//                {
//                    OrderID = i,
//                    CustomerID = CustomerID[random.Next(7)],
//                    EmployeeID = random.Next(1700, 1800).ToString(),
//                    FirstName = FirstNames[random.Next(7)],
//                    LastName = LastNames[random.Next(7)]
//                };
//                orderDetails.Add(order);
//            }
//            return orderDetails;
//        }
//
//        public OrderInfo RefreshItemsource(int i)
//        {
//            var order = new OrderInfo()
//            {
//                OrderID = i,
//                CustomerID = CustomerID[random.Next(7)],
//                EmployeeID = random.Next(1700, 1800).ToString(),
//                FirstName = FirstNames[random.Next(7)],
//                LastName = LastNames[random.Next(7)]
//            };
//            return order;
//        }

        //Main DataSources
        string[] FirstNames = new string[] {
            "Kyle",
            "Gina",
            "Irene",
            "Katie",
            "Michael",
            "Oscar",
            "Ralph"
        };

        string[] LastNames = new string[] {
            "Adams",
            "Crowley",
            "Ellis",
            "Gable",
            "Irvine",
            "Keefe",
            "Mendoza"
        };

        string[] CustomerID = new string[] {
            "Hanna",
            "Frans",
            "Maria",
            "John",
            "Andrew",
            "Fuller",
            "Carter"
        };

        private void GenerateOrders()
        {
            orderInfo.Add(new OrderInfo(1001, "Maria Anders", "Germany", "ALFKI", "Berlin"));
            orderInfo.Add(new OrderInfo(1002, "Ana Trujillo", "Mexico", "ANATR", "Mexico D.F."));
            orderInfo.Add(new OrderInfo(1003, "Ant Fuller", "Mexico", "ANTON", "Mexico D.F."));
            orderInfo.Add(new OrderInfo(1004, "Thomas Hardy", "UK", "AROUT", "London"));
            orderInfo.Add(new OrderInfo(1005, "Tim Adams", "Sweden", "BERGS", "London"));
            orderInfo.Add(new OrderInfo(1006, "Hanna Moos", "Germany", "BLAUS", "Mannheim"));
            orderInfo.Add(new OrderInfo(1007, "Andrew Fuller", "France", "BLONP", "Strasbourg"));
            orderInfo.Add(new OrderInfo(1008, "Martin King", "Spain", "BOLID", "Madrid"));
            orderInfo.Add(new OrderInfo(1009, "Lenny Lin", "France", "BONAP", "Marsiella"));
            orderInfo.Add(new OrderInfo(1010, "John Carter", "Canada", "BOTTM", "Lenny Lin"));
            orderInfo.Add(new OrderInfo(1011, "Laura King", "UK", "AROUT", "London"));
            orderInfo.Add(new OrderInfo(1012, "Anne Wilson", "Germany", "BLAUS", "Mannheim"));
            orderInfo.Add(new OrderInfo(1013, "Martin King", "France", "BLONP", "Strasbourg"));
            orderInfo.Add(new OrderInfo(1014, "Gina Irene", "UK", "AROUT", "London"));
        }
    }
}
