﻿//-----------------------------------------------------------------------
// <copyright file="InventoryConfiguration.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <summary>
    /// A representation of the required reader Inventory parameter configuration
    /// </summary>
    public class InventoryConfiguration
    {
        /// <summary>
        /// Gets or sets the antenna output power
        /// </summary>
        public int OutputPower { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the RSSI should be reported
        /// </summary>
        public bool IsRssiRequested { get; set; }
    }
}
