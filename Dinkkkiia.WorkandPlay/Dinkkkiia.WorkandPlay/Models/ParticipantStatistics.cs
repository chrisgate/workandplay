﻿//-----------------------------------------------------------------------
// <copyright file="InventoryStatistics.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Timers;
using GalaSoft.MvvmLight;

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <summary>
    /// Container for statistics relating to the inventory passes
    /// </summary>
    public class ParticipantStatistics
        : ObservableObject
    {

        /// <summary>
        /// Gets the start date time.
        /// </summary>
        public DateTime StartDateTime { get; private set; }
        /// <summary>
        /// Gets the start date time.
        /// </summary>
        public DateTime SportStartDateTime { get; private set; } = DateTime.Now;

        /// <summary>
        /// Gets the remain time in seconds.
        /// </summary>
        public double RemainTime
        {
            get { return remainTime; }

            private set
            {
                //                remainTime = value;
                //                OnPropertyChanged();
                this.Set(ref this.remainTime, value);

            }
        }
        /// <summary>
        /// Gets the remain time in seconds.
        /// </summary>
        public double TimeTicking
        {
            get { return remainTime; }

            private set
            {
                //                remainTime = value;
                //                OnPropertyChanged();
                this.Set(ref this.remainTime, value);

            }
        }
        /// <summary>
        /// Gets the remain time in seconds.
        /// </summary>
        public double LapTime
        {
            get { return lapTime; }

            private set
            {
                //                remainTime = value;
                //                OnPropertyChanged();
                this.Set(ref this.lapTime, value);

            }
        }

        /// <summary>
        /// Occurs when completed.
        /// </summary>
        public event Action Completed;


        /// Occurs when ticked.
        /// </summary>
        public event Action Ticked;

        /// <summary>
        /// The timer.
        /// </summary>
        Timer timer;

        /// <summary>
        /// The remain time.
        /// </summary>
        double remainTime;

        /// <summary>
        /// The remain time.
        /// </summary>
        double timeTicker;
        /// <summary>
        /// The remain time.
        /// </summary>
        double lapTime;

        /// <summary>
        /// The remain time total.
        /// </summary> 

        /// <summary>
        /// The remain time total.
        /// </summary>
        double remainTimeTotal;

        /// <summary>
        /// The initial time.
        /// </summary>
        double initialTime = 0.0;

        /// <summary>
        /// Starts the updating participant time and period are specified in seconds.
        /// </summary>
        public void StartTiming(double period = 1.0)
        {
            if (timer != null)
            {
                StopUpdating();
            }

            //            remainTimeTotal = total;
            //            RemainTime = total;

            StartDateTime = DateTime.Now;

            timer = new Timer(period * 1000);
            timer = new Timer();
            //timer.Elapsed += (sender, e) => Tick();
            timer.Elapsed += (sender, e) => SportTick();
            timer.Enabled = true;
        }
        /// <summary>
        /// Starts the updating with specified period, total time and period are specified in seconds.
        /// </summary>
        public void StartUpdating(double total, double period = 1.0)
        {
            if (timer != null)
            {
                StopUpdating();
            }

            //            remainTimeTotal = total;
            //            RemainTime = total;

            StartDateTime = DateTime.Now;

            timer = new Timer(period * 1000);
            timer = new Timer();
            //timer.Elapsed += (sender, e) => Tick();
            timer.Elapsed += (sender, e) => SportTick();
            timer.Enabled = true;
        }

        /// <summary>
        /// Stops the updating.
        /// </summary>
        public void StopUpdating()
        {
            RemainTime = 0;
            remainTimeTotal = 0;

            if (timer != null)
            {
                timer.Enabled = false;
                timer = null;
            }
        }

        /// <summary>
        /// Updates the time remain.
        /// </summary>
        public void Tick()
        {
            var delta = (DateTime.Now - StartDateTime).TotalSeconds;

            if (delta < remainTimeTotal)
            {
                RemainTime = remainTimeTotal - delta;

                var ticked = Ticked;
                if (ticked != null)
                {
                    ticked();
                }
            }
            else
            {
                RemainTime = 0;

                var completed = Completed;
                if (completed != null)
                {
                    completed();
                }
            }
        }
        /// <summary>
        /// Updates the time remain.
        /// </summary>
        public void SportTick()
        {
            //var delta = (DateTime.Now - StartDateTime).TotalSeconds;
            var delta = (DateTime.Now - StartDateTime).TotalSeconds;

            if (delta >= 0.0)
            {
                TimeTicking = delta;

                var ticked = Ticked;
                if (ticked != null)
                {
                    ticked();
                }
            }
            else
            {
                TimeTicking = 0;

                var completed = Completed;
                if (completed != null)
                {
                    completed();
                }
            }
        }




        /// <summary>
        /// Backing field for InventoryMode
        /// </summary>
        private string inventoryMode = string.Empty;

        /// <summary>
        /// Backing field for CurrentScanSeenCount
        /// </summary>
        private int currentScanSeenCount;

        /// <summary>
        /// Backing field for CurrentScanUniqueCount
        /// </summary>
        private int currentScanUniqueCount;

        /// <summary>
        /// Backing field for last scan unique count
        /// </summary>
        private int lastScanUniqueCount;

        /// <summary>
        /// Backing field for LastScanSeenCount
        /// </summary>
        private int lastScanSeenCount;

        /// <summary>
        /// Backing field for NumberOfScans
        /// </summary>
        private int numberOfScans;

        /// <summary>
        /// Backing field for TotalSeenCount
        /// </summary>
        private int totalSeenCount;

        /// <summary>
        /// Backing field for TotalUniqueCount
        /// </summary>
        private int totalUniqueCount;
        
        /// <summary>
        /// Gets or sets the InventoryMode for the inventory
        /// </summary>
        public string InventoryMode
        { 
            get
            {
                return this.inventoryMode;
            }

            set
            {
                this.Set(ref this.inventoryMode, value);
            }
        }

        /// <summary>
        /// Gets or sets the total number of transponders seen in this inventory pass
        /// </summary>
        public int CurrentScanSeenCount
        {
            get
            {
                return this.currentScanSeenCount;
            }

            set
            {
                this.Set(ref this.currentScanSeenCount, value);
            }
        }

        /// <summary>
        /// Gets or sets the number of new unique identifiers added in this inventory pass
        /// </summary>
        public int CurrentScanUniqueCount
        {
            get
            {
                return this.currentScanUniqueCount;
            }

            set
            {
                this.Set(ref this.currentScanUniqueCount, value);
            }
        }

        /// <summary>
        /// Gets or sets the total number of transponders since last reset
        /// </summary>
        public int TotalUniqueCount
        {
            get
            {
                return this.totalUniqueCount;
            }

            set
            {
                this.Set(ref this.totalUniqueCount, value);
            }
        }

        /// <summary>
        /// Gets or sets the total number of identifies seen since the statistics were last reset
        /// </summary>
        public int TotalSeenCount
        {
            get
            {
                return this.totalSeenCount;
            }

            set
            {
                this.Set(ref this.totalSeenCount, value);
            }
        }

        /// <summary>
        /// Gets or sets the number of inventory passes completed
        /// </summary>
        public int NumberOfScans
        {
            get
            {
                return this.numberOfScans;
            }

            set
            {
                this.Set(ref this.numberOfScans, value);
            }
        }

        /// <summary>
        /// Gets or sets the number of unique transponders added in the last complete pass
        /// </summary>
        public int LastScanUniqueCount
        {
            get
            {
                return this.lastScanUniqueCount;
            }

            set
            {
                this.Set(ref this.lastScanUniqueCount, value);
            }
        }

        /// <summary>
        /// Gets or sets the number of transponders seen in the last pass
        /// </summary>
        public int LastScanSeenCount
        {
            get
            {
                return this.lastScanSeenCount;
            }

            set
            {
                this.Set(ref this.lastScanSeenCount, value);
            }
        }

        /// <summary>
        /// Clears the current statistics
        /// </summary>
        public void Clear()
        {
            this.CurrentScanSeenCount = 0;
            this.CurrentScanUniqueCount = 0;
            this.InventoryMode = string.Empty;
            this.LastScanSeenCount = 0;
            this.LastScanUniqueCount = 0;
            this.NumberOfScans = 0;
            this.TotalSeenCount = 0;
            this.TotalUniqueCount = 0;
        }

        /// <summary>
        /// Updates the statistics with a partial result
        /// </summary>
        /// <param name="unique">The number of unique transponders seen since the last update</param>
        /// <param name="seen">The total number of transponders seen since the last update</param>
        /// <param name="endPass">True if this completes the inventory; False otherwise. i.e. to reset the Scan counts</param>
        public void Update(int unique, int seen, bool endPass)
        {
            this.CurrentScanSeenCount += seen;
            this.CurrentScanUniqueCount += unique;
            this.TotalSeenCount += seen;
            this.TotalUniqueCount += unique;

            if (endPass)
            {
                this.EndPass();
            }
        }

        /// <summary>
        /// Copies the current scan values to the last scan values.
        /// Resets the current scan values and increments the scan count
        /// </summary>
        public void EndPass()
        {
            this.LastScanSeenCount = this.CurrentScanSeenCount;
            this.LastScanUniqueCount = this.CurrentScanUniqueCount;
            this.CurrentScanSeenCount = 0;
            this.CurrentScanUniqueCount = 0;
            this.NumberOfScans += 1;
        }
    }
}
