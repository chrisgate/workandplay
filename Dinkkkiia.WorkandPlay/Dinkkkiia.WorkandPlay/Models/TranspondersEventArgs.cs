﻿//-----------------------------------------------------------------------
// <copyright file="TranspondersEventArgs.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <summary>
    /// Represents a collection of transponders reported from an inventory
    /// </summary>
    public class TranspondersEventArgs
        : EventArgs
    {
        /// <summary>
        /// Gets or sets the transponders being reported
        /// </summary>
        public IEnumerable<TechnologySolutions.Rfid.AsciiProtocol.TransponderData> Transponders { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this completes an inventory (true) or whether there are more transponders to follow (false)
        /// </summary>
        public bool EndOfPass { get; set; }
    }
}
