﻿//-----------------------------------------------------------------------
// <copyright file="TransponderInventory.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Collections.ObjectModel;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Services;

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <summary>
    /// View model for the inventory of transponders and barcodes
    /// </summary>
    public class TransponderInventory
    {

        /// <summary>
        /// Cache used to identify unique transponders
        /// </summary>
        private IDictionary<string, IdentifiedItem> transponders;

        /// <summary>
        /// Used to invoke actions on the user interface thread
        /// </summary>
        private IDispatcher dispatcher;

        /// <summary>
        /// Used to normalize the RSSI signal
        /// </summary>
        private ISignalNormalization signal;

        /// <summary>
        /// Initializes a new instance of the TransponderInventory class
        /// </summary>
        /// <param name="statistics">The model to update with inventory statistics</param>
        /// <param name="monitorTransponders">Reports transponders inventory</param>
        /// <param name="signal">Used to normalize RSSI values</param>
        /// <param name="dispatcher">Used to invoke actions on the user interface thread</param>
        public TransponderInventory(InventoryStatistics statistics, IMonitorTransponders monitorTransponders, ISignalNormalization signal, IDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.signal = signal;
            this.Statistics = statistics;
            this.Identifiers = new ObservableCollection<IdentifiedItem>();
            this.transponders = new Dictionary<string, IdentifiedItem>();
            this.IsEnabled = true;

            monitorTransponders.TranspondersReceived += (sender, e) =>
            {
                if (this.IsEnabled)
                {
                    this.dispatcher.InvokeOnUserInterfaceThread(() =>
                       {
                           this.AddTransponders(e.Transponders, e.EndOfPass);
                       });
                }
            };
        }

        /// <summary>
        /// Gets or sets a value indicating whether to append received transponders to the Identifiers collection
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Gets the statistics for the transponders scanned
        /// </summary>
        public InventoryStatistics Statistics { get; private set; }

        /// <summary>
        /// Gets the transponders scanned
        /// </summary>
        public ObservableCollection<IdentifiedItem> Identifiers { get; private set; }

       /// <summary>
        /// Adds the transponders to the displayed list updating the statistics
        /// </summary>
        /// <param name="transponders">The transponders to add</param>
        /// <param name="endPass">True if this update is the last for this inventory pass</param>
        public void AddTransponders(IEnumerable<TechnologySolutions.Rfid.AsciiProtocol.TransponderData> transponders, bool endPass)
        {
            int unique;
            int seen;            

            seen = 0;
            unique = 0;

            foreach (var transponder in transponders)
            {
                var rssi = transponder.Rssi.HasValue ? this.signal.Normalize(transponder.Rssi.Value) : IdentifiedItem.NoSignal;

                seen += 1;
                if (this.transponders.ContainsKey(transponder.Epc))
                {
                    this.transponders[transponder.Epc].Seen(rssi, transponder.Timestamp);
                }
                else
                {
                    IdentifiedItem newItem;
                    unique += 1;

                    newItem = new IdentifiedItem(transponder.Epc, transponder.Timestamp, rssi);
                    this.transponders.Add(newItem.Identifier, newItem);
                    this.Identifiers.Add(newItem);
                }
            }

            this.Statistics.Update(unique, seen, endPass);
        }

        /// <summary>
        /// Reset all of the statistics
        /// </summary>
        public void Clear()
        {
            this.Identifiers.Clear();
            this.Statistics.Clear();
            this.transponders.Clear();
            this.signal.Reset();
        }        
    }
}
