﻿//-----------------------------------------------------------------------
// <copyright file="TransponderInventory.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Collections.ObjectModel;
using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Dinkkkiia.WorkandPlay.Services;

namespace Dinkkkiia.WorkandPlay.Models
{
    /// <summary>
    /// View model for the Counting of Participant 
    /// </summary>
    public class ParticipantsCounter
    {

        /// <summary>
        /// Cache used to identify unique Participants
        /// </summary>
        private IDictionary<string, Participant> participants;

        /// <summary>
        /// Used to invoke actions on the user interface thread
        /// </summary>
        private IDispatcher dispatcher;

        /// <summary>
        /// Used to normalize the RSSI signal
        /// </summary>
        private ISignalNormalization signal;

        /// <summary>
        /// Initializes a new instance of the ParticipantsCounter class
        /// </summary>
        /// <param name="statistics">The model to update with Counting statistics</param>
        /// <param name="monitorTransponders">Reports Participant Counting</param>
        /// <param name="signal">Used to normalize RSSI values</param>
        /// <param name="dispatcher">Used to invoke actions on the user interface thread</param>
        public ParticipantsCounter(InventoryStatistics statistics, IMonitorTransponders monitorTransponders, ISignalNormalization signal, IDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.signal = signal;
            this.Statistics = statistics;
            this.Participants = new ObservableCollection<Participant>();
            this.participants = new Dictionary<string, Participant>();
            this.IsEnabled = true;

            monitorTransponders.TranspondersReceived += (sender, e) =>
            {
                if (this.IsEnabled)
                {
                    this.dispatcher.InvokeOnUserInterfaceThread(() =>
                       {
                           this.AddTransponders(e.Transponders, e.EndOfPass);
                       });
                }
            };
        }

        /// <summary>
        /// Gets or sets a value indicating whether to append received transponders to the Identifiers collection
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Gets the statistics for the transponders scanned
        /// </summary>
        public InventoryStatistics Statistics { get; private set; }

        /// <summary>
        /// Gets the transponders scanned
        /// </summary>
        public ObservableCollection<Participant> Participants { get; private set; }

       /// <summary>
        /// Adds the transponders to the displayed list updating the statistics
        /// </summary>
        /// <param name="transponders">The transponders to add</param>
        /// <param name="endPass">True if this update is the last for this inventory pass</param>
        public void AddTransponders(IEnumerable<TechnologySolutions.Rfid.AsciiProtocol.TransponderData> transponders, bool endPass)
        {
            int unique;
            int seen;            

            seen = 0;
            unique = 0;

            foreach (var transponder in transponders)
            {
                var rssi = transponder.Rssi.HasValue ? this.signal.Normalize(transponder.Rssi.Value) : IdentifiedItem.NoSignal;

                seen += 1;
                if (this.participants.ContainsKey(transponder.Epc))
                {
                    this.participants[transponder.Epc].Seen(rssi, transponder.Timestamp);
                }
                else
                {
                    Participant newParticipant;
                    unique += 1;

                    newParticipant = new Participant(transponder.Epc, transponder.Timestamp, rssi);
                    this.participants.Add(newParticipant.Identifier, newParticipant);
                    this.Participants.Add(newParticipant);
                }
            }

            this.Statistics.Update(unique, seen, endPass);
        }

        /// <summary>
        /// Reset all of the statistics
        /// </summary>
        public void Clear()
        {
            this.Participants.Clear();
            this.Statistics.Clear();
            this.participants.Clear();
            this.signal.Reset();
        }        
    }
}
