﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dinkkkiia.WorkandPlay.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
        }
    }
}
