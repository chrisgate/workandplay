﻿using Xamarin.Forms;

namespace Dinkkkiia.WorkandPlay.Views
{
    public partial class ChangeEpcPage : ContentPage
    {
        public ChangeEpcPage()
        {
            InitializeComponent();
            this.BindingContext = App.Locator.ChangeEpcView;
        }
    }
}
