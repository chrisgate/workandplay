﻿//-----------------------------------------------------------------------
// <copyright file="ReadWritePage.xaml.cs" company="Technology Solutions UK Ltd">
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved.
// </copyright>
// <author></author>
//-----------------------------------------------------------------------

using Xamarin.Forms;

namespace Dinkkkiia.WorkandPlay.Views
{
    /// <summary>
    /// XAML behind code
    /// </summary>
    public partial class ReadWritePage : ContentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadWritePage" /> class
        /// </summary>
        public ReadWritePage()
        {
            this.InitializeComponent();
            this.BindingContext = App.Locator.ChangeRWView;
        }
    }
}
