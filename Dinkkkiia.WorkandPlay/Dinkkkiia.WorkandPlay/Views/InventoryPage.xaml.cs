﻿using Xamarin.Forms;

namespace Dinkkkiia.WorkandPlay.Views
{
    public partial class InventoryPage : ContentPage
    {
        public InventoryPage()
        {
            InitializeComponent();
            this.BindingContext = App.Locator.InventoryView;
        }
    }
}
