﻿using Dinkkkiia.WorkandPlay.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dinkkkiia.WorkandPlay.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SportyTimer : ContentPage
    {
        static Countdown countdown;

        public SportyTimer()
        {
            InitializeComponent();
            var label = new Label
            {
                Text = "Hello, Forms!",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };


            countdown = new Countdown();





            countdown.StartUpdating(3600 * 24);

            label.SetBinding(Label.TextProperty,

                new Binding("RemainTime", BindingMode.Default, new CountdownConverter()));
            label.BindingContext = countdown;

            Content = label;


        }
    }
}