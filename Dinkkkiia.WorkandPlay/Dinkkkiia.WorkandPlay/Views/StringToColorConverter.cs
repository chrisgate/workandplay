﻿//-----------------------------------------------------------------------
// <copyright file="StringToColorConverter.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Globalization;
using Xamarin.Forms;

namespace Dinkkkiia.WorkandPlay.Views
{
    /// <summary>
    /// Converts a bound name to a <see cref="Color"/>
    /// </summary>
    /// <remarks>
    /// Based on the following
    /// http://smartmobidevice.blogspot.co.uk/2015/04/stringtocolorconverter-for-xamarinforms.html
    /// </remarks>
    public class StringToColorConverter : IValueConverter
    {
        /// <summary>
        /// Implement this method to convert value to targetType by using parameter and culture.
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <param name="targetType">The type to convert to</param>
        /// <param name="parameter">The parameter provided to the converter if any</param>
        /// <param name="culture">The culture to use</param>
        /// <returns>The <see cref="Color"/> for the specified color name</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string valueAsString = value.ToString();
            Color result;

            switch (valueAsString)
            {
                case "":
                    result = Color.Default;
                    break;                                
                    
                case "Accent":
                    result = Color.Accent;
                    break;

                case "Blue":
                    result = Color.Aqua;
                    break;

                case "Red":
                    result = Color.Red;
                    break;

                case "LightGray":
                    result = Color.Silver;
                    break;

                case "Green":
                    result = Color.Lime;
                    break;
                    
                default:
                    result = Color.FromHex(valueAsString);
                    break;                    
            }

            return result;
        }

        /// <summary>
        /// Implement this method to convert value back from targetType by using parameter and culture.
        /// </summary>
        /// <param name="value">The value to convert</param>
        /// <param name="targetType">The type to convert to</param>
        /// <param name="parameter">The parameter provided to the converter if any</param>
        /// <param name="culture">The culture to use</param>
        /// <returns>null as not implemented</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
