﻿using System;
using Xamarin.Forms;

namespace Dinkkkiia.WorkandPlay.Views
{
/// <summary>
/// Converts countdown seconds double value to string "HH : MM : SS"
/// </summary>
public class CountdownConverter : IValueConverter
{
#region IValueConverter implementation

public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
{
    var timespan = TimeSpan.FromSeconds((double)value);

    if (timespan.TotalSeconds < 1.0)
    {
    return "-- : --";
    }
    //            else if (timespan.TotalSeconds < 3600)
    //            {
    //                return string.Format("{0:D2} : {1:D2}",
    //                    timespan.Minutes, timespan.Seconds);
    //            }
    else if (timespan.TotalSeconds > 3600 * 24)
    {
    return "24 : 00 : 00";
    }

//        return string.Format("{0:D2} : {1:D2} : {2:D2}",
//         timespan.Hours, timespan.Minutes, timespan.Seconds);
//    var millisec = timespan.Milliseconds.ToString().Substring(0, 2);
           return string.Format("{0} : {1:D2} : {2:D2} :{3:D2}", timespan.Milliseconds,
         timespan.Seconds, timespan.Minutes, timespan.Hours);
//        return timespan.ToString(@"dd\.hh\:mm\:ss:\ff");

        }
    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    #endregion
    }
}
