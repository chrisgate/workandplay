﻿//-----------------------------------------------------------------------
// <copyright file="ILifeCycle.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------
namespace Dinkkkiia.WorkandPlay.Common.ViewModels
{
    /// <summary>
    /// When implemented by a view provides methods that are called for view events
    /// </summary>
    public interface ILifeCycle
    {
        /// <summary>
        /// Called when the view is displayed
        /// </summary>
        void Shown();

        /// <summary>
        /// Called when the view is hidden
        /// </summary>
        void Hidden();
    }
}
