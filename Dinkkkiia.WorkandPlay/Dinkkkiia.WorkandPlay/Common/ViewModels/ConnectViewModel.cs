﻿//-----------------------------------------------------------------------
// <copyright file="ConnectViewModel.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Dinkkkiia.WorkandPlay.Common.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using TechnologySolutions.Rfid.AsciiProtocol.Extensions;

namespace Dinkkkiia.WorkandPlay.Common.ViewModels
{
    // using GalaSoft.MvvmLight.Views;

    /// <summary>
    /// View model to see the available connections and connect to a reader
    /// </summary>
    public class ConnectViewModel
        : ViewModelBase, ILifeCycle
    {
        /// <summary>
        /// Used to navigate to other view models
        /// </summary>
        private INavigationService navigator;

        /// <summary>
        /// Performs the connection management
        /// </summary>
        private IReaderConnectionManager readerConnectionManager;

        /// <summary>
        /// The currently selected reader
        /// </summary>
        private INamedReader selectedReader;

        /// <summary>
        /// Backing field for <see cref="ErrorMessage"/>
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// Backing field for <see cref="InformationalMessage"/>
        /// </summary>
        private string informationalMessage;

        /// <summary>
        /// Backing field for <see cref="IsBusy"/>
        /// </summary>
        private bool isBusy = false;

        /// <summary>
        /// Initializes a new instance of the ConnectViewModel class
        /// </summary>
        /// <param name="navigator">Provides navigation to other view models</param>
        /// <param name="readerConnectionManager">Used to connect to the reader</param>
        public ConnectViewModel(INavigationService navigator, IReaderConnectionManager readerConnectionManager)
        {
            this.navigator = navigator;
            this.readerConnectionManager = readerConnectionManager;

            this.Readers = new ObservableCollection<INamedReader>();
            this.AddNewCommand = new RelayCommand(this.ExecuteAddNew);
            this.BackCommand = new RelayCommand(this.navigator.GoBack);
            this.ConnectCommand = new RelayCommand(this.ExecuteConnect, this.CanExecuteConnect);
            this.DisconnectCommand = new RelayCommand(this.ExecuteDisconnect, this.CanExecuteDisconnect);
            this.ItemSelectedCommand = new RelayCommand(this.ExecuteItemSelected);
            this.RefreshListCommand = new RelayCommand(this.ExecuteRefreshList);

            this.ErrorMessage = null;
        }

        /// <summary>
        /// Gets the key for the <see cref="INavigationService"/> to navigate to the view for this ViewModel
        /// </summary>
        public static string NavigationKey
        {
            get
            {
                return "ConnectPage";
            }
        }

        /// <summary>
        /// Gets the collection of available readers
        /// </summary>
        public ObservableCollection<INamedReader> Readers { get; private set; }

        /// <summary>
        /// Gets or sets the selection in the list of available readers
        /// </summary>
        public INamedReader SelectedReader
        {
            get
            {
                return this.selectedReader;
            }

            set
            {
                if (this.Set(ref this.selectedReader, value))
                {
                    this.UpdateCommands();
                }
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.Set(ref this.errorMessage, value);
            }
        }

        /// <summary>
        /// Gets or sets the informational message
        /// </summary>
        public string InformationalMessage
        {
            get
            {
                return this.informationalMessage;
            }

            set
            {
                this.Set(ref this.informationalMessage, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the view mode is busy
        /// </summary>
        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                this.Set(ref this.isBusy, value);
            }
        }

        /// <summary>
        /// Gets the command to add a new  reader to the list
        /// </summary>
        public ICommand AddNewCommand { get; private set; }

        /// <summary>
        /// Gets the command to return to the previous screen
        /// </summary>
        public ICommand BackCommand { get; private set; }

        /// <summary>
        /// Gets the connect to the selected reader
        /// </summary>
        public ICommand ConnectCommand { get; private set; }

        /// <summary>
        /// Gets the command to disconnect from the current reader
        /// </summary>
        public ICommand DisconnectCommand { get; private set; }

        /// <summary>
        /// Gets the command that is called as an item is selected
        /// </summary>
        public ICommand ItemSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to refresh the list of readers
        /// </summary>
        public ICommand RefreshListCommand { get; private set; }

        /// <summary>
        /// Called as the view bound to this view model is shown
        /// </summary>
        public void Shown()
        {
            this.ErrorMessage = null;
            this.InformationalMessage = null;
            this.IsBusy = true;
            this.RefreshListCommand.Execute(null);
        }

        /// <summary>
        /// Called as the view bound to this view model is hidden
        /// </summary>
        public void Hidden()
        {
        }

        /// <summary>
        /// Returns a value indicating whether the <see cref="ConnectCommand"/> can execute
        /// </summary>
        /// <returns>Whether the connect command can execute</returns>
        private bool CanExecuteConnect()
        {
            return this.SelectedReader != null && this.readerConnectionManager != null && this.readerConnectionManager.ConnectedReader == null;
        }

        /// <summary>
        /// Returns a value indicating whether the <see cref="DisconnectCommand"/> can execute
        /// </summary>
        /// <returns>Whether the disconnect command can execute</returns>
        private bool CanExecuteDisconnect()
        {
            return this.readerConnectionManager != null && this.readerConnectionManager.ConnectedReader != null;
        }

        /// <summary>
        /// Performs the <see cref="AddNewCommand"/>
        /// </summary>
        private void ExecuteAddNew()
        {
            try
            {
                this.ErrorMessage = null;
                this.InformationalMessage = null;

                if (this.readerConnectionManager != null)
                {
                    this.readerConnectionManager.AddNewReader();
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            finally
            {
                this.UpdateCommands();
            }
        }

        /// <summary>
        /// Performs the <see cref="ConnectCommand"/>
        /// </summary>
        private async void ExecuteConnect()
        {
            try
            {
                this.IsBusy = true;
                this.ErrorMessage = null;
                this.InformationalMessage = "Connecting";

                await this.readerConnectionManager.ConnectAsync(this.SelectedReader);

                this.InformationalMessage = "Connected";
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                this.InformationalMessage = "Connect failed!";
            }
            finally
            {
                this.IsBusy = false;
                this.UpdateCommands();
            }
        }

        /// <summary>
        /// Performs the <see cref="DisconnectCommand"/>
        /// </summary>
        private void ExecuteDisconnect()
        {
            try
            {
                this.IsBusy = true;
                this.ErrorMessage = null;
                this.InformationalMessage = null;

                this.readerConnectionManager.Disconnect();

                this.InformationalMessage = "Disconnected";
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                this.InformationalMessage = "Disconnect failed!";
            }
            finally
            {
                this.IsBusy = false;
                this.UpdateCommands();
            }
        }

        /// <summary>
        /// Performs the <see cref="ItemSelectedCommand"/>
        /// </summary>
        private void ExecuteItemSelected()
        {
            this.UpdateCommands();
        }

        /// <summary>
        /// Performs the <see cref="RefreshListCommand"/>
        /// </summary>
        private async void ExecuteRefreshList()
        {
            try
            {
                this.IsBusy = true;
                this.ErrorMessage = null;

                var currentReaders = await this.readerConnectionManager.ListAvailableReadersAsync();
                currentReaders = currentReaders.ToList();

                // Determine the additions and removals
                var unchangedAvailableReaders = this.Readers.Join(currentReaders, x => x.DisplayName, y => y.DisplayName, (x, y) => x);
                var unchangedPairedReaders = this.Readers.Join(currentReaders, x => x.DisplayName, y => y.DisplayName, (x, y) => y);
                var removed = this.Readers.Except(unchangedAvailableReaders).ToList();
                var added = currentReaders.Except(unchangedPairedReaders).ToList();

                // Change the contents of the list rather than replacing it
                // so that any bindings are not lost
                // Remove unavailable readers
                foreach (var reader in removed)
                {
                    this.Readers.Remove(reader);
                }

                // Add newly available readers
                foreach (var reader in added)
                {
                    this.Readers.Add(reader);
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            finally
            {
                this.IsBusy = false;
                this.UpdateCommands();
            }
        }

        /// <summary>
        /// Updates the <see cref="ICommand"/>s CanExecute to reflect a new state
        /// </summary>
        private void UpdateCommands()
        {
            (this.ConnectCommand as RelayCommand).RaiseCanExecuteChanged();
            (this.DisconnectCommand as RelayCommand).RaiseCanExecuteChanged();
        }        
    }
}
