﻿//-----------------------------------------------------------------------
// <copyright file="Locate.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using GalaSoft.MvvmLight.Ioc;

namespace Dinkkkiia.WorkandPlay.Common.ViewModels
{
    /// <summary>
    /// Provides a locator for view models
    /// </summary>
    public static class Locate
    {
        /// <summary>
        /// Returns the requested view model instance
        /// </summary>
        /// <typeparam name="TViewModel">The required view model type</typeparam>
        /// <returns>The view model instance</returns>
        public static TViewModel ViewModel<TViewModel>()
        {
            return SimpleIoc.Default.GetInstance<TViewModel>();
        }
    }
}