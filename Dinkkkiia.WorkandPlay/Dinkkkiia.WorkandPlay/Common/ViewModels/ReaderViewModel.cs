﻿//-----------------------------------------------------------------------
// <copyright file="ReaderViewModel.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System.Windows.Input;
using Dinkkkiia.WorkandPlay.Common.Models;
using Dinkkkiia.WorkandPlay.Common.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Dinkkkiia.WorkandPlay.Common.ViewModels
{
    // using GalaSoft.MvvmLight.Views;

    /// <summary>
    /// ViewModel to display reader information. Also provides navigation to change the connected reader
    /// </summary>
    public class ReaderViewModel
        : ViewModelBase
    {
        /// <summary>
        /// Holds a reference to the navigator to allow the ViewModel to navigate to others
        /// </summary>
        private INavigationService navigator;

        /// <summary>
        /// Initializes a new instance of the ReaderViewModel class
        /// </summary>
        /// <param name="navigator">Used to navigate to the </param>
        /// <param name="information">The information about the currently connected reader</param>        
        public ReaderViewModel(INavigationService navigator, ReaderInformation information)
        {
            this.navigator = navigator;

            this.ConnectCommand = new RelayCommand(this.ExecuteConnect);
            this.Information = information;
        }        

        /// <summary>
        /// Gets the command to connect to a reader
        /// </summary>
        public ICommand ConnectCommand { get; private set; }

        /// <summary>
        /// Gets the information about the currently connected reader
        /// </summary>
        public ReaderInformation Information { get; private set; }

        /// <summary>
        /// Performs the <see cref="ConnectCommand"/>
        /// </summary>
        public void ExecuteConnect()
        {
            this.navigator.NavigateTo(ConnectViewModel.NavigationKey);
        }
    }
}
