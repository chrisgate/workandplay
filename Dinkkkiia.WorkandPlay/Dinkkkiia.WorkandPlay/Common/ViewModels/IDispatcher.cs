﻿//-----------------------------------------------------------------------
// <copyright file="IDispatcher.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;

namespace Dinkkkiia.WorkandPlay.Common.ViewModels
{
    /// <summary>
    /// Provides a method to invoke actions on the user interface thread
    /// </summary>
    public interface IDispatcher
    {
        /// <summary>
        /// Invokes action on the user interface thread
        /// </summary>
        /// <param name="action">The action to perform on the user interface thread</param>
        void InvokeOnUserInterfaceThread(Action action);
    }
}
