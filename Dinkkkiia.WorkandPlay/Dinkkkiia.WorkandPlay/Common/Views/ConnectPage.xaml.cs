﻿using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dinkkkiia.WorkandPlay.Common.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConnectPage : ContentPage
    {
        public ConnectPage()
        {
            InitializeComponent();
            this.BindWithLifeCyle(Locate.ViewModel<ConnectViewModel>());
            //this.BindWithLifeCyle(ViewModelLocator.ConnectView);

        }
    }
}