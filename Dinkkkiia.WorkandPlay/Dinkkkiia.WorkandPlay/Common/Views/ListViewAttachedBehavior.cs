﻿//-----------------------------------------------------------------------
// <copyright file="ListViewAttachedBehavior.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <source>http://stackoverflow.com/questions/26040652/binding-to-listview-item-tapped-property-from-view-model</source>
//-----------------------------------------------------------------------

using System.Windows.Input;
using Xamarin.Forms;

namespace Dinkkkiia.WorkandPlay.Common.Views
{
    /// <summary>
    /// Adds a Command binding to a list view for when an item in the list is clicked
    /// </summary>
    public static class ListViewAttachedBehavior
    {
        /// <summary>
        /// The Command property to bind to
        /// </summary>
        public static readonly BindableProperty CommandProperty =
            BindableProperty.CreateAttached(
            "Command",
            typeof(ICommand),
            typeof(ListViewAttachedBehavior),
            null,
            propertyChanged: OnCommandChanged);

        /// <summary>
        /// As the command property is updated register the ItemTapped event of the list to trigger the command
        /// </summary>
        /// <param name="view">The list view that raises the ItemTapped event</param>
        /// <param name="oldValue">The old value for Command</param>
        /// <param name="newValue">The new value for Command</param>
        public static void OnCommandChanged(BindableObject view, object oldValue, object newValue)
        {
            var entry = view as ListView;
            if (entry == null)
            {
                return;
            }

            entry.ItemTapped += (sender, e) =>
            {
                var command = newValue as ICommand;
                if (command == null)
                {
                    return;
                }

                if (command.CanExecute(e.Item))
                {
                    command.Execute(e.Item);
                }
            };
        }
    }
}
