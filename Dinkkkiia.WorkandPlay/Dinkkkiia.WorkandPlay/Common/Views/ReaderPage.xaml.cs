﻿using Dinkkkiia.WorkandPlay.Common.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dinkkkiia.WorkandPlay.Common.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReaderPage : ContentPage
    {
        public ReaderPage()
        {
            InitializeComponent();
            this.BindingContext = Locate.ViewModel<ReaderViewModel>();

        }
    }
}