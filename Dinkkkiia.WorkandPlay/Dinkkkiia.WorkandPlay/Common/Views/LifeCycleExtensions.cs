﻿//-----------------------------------------------------------------------
// <copyright file="LifeCycleExtensions.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using Dinkkkiia.WorkandPlay.Common.ViewModels;

namespace Dinkkkiia.WorkandPlay.Common.Views
{
    /// <summary>
    /// Extension methods for <see cref="ILifeCycle"/>
    /// </summary>
    public static class LifeCycleExtensions
    {
        /// <summary>
        /// Set the BindingContext of page to the specified viewModel but also links the page's lifecycle events to the viewModel's interface
        /// </summary>
        /// <param name="page">The page to be bound</param>
        /// <param name="viewModel">The view model to bind to</param>
        public static void BindWithLifeCyle(this Xamarin.Forms.Page page, ILifeCycle viewModel)
        {
            page.BindingContext = viewModel;
            page.Appearing += (sender, e) => { viewModel.Shown(); };
            page.Disappearing += (sender, e) => { viewModel.Hidden(); };            
        }
    }
}