﻿//-----------------------------------------------------------------------
// <copyright file="ReaderInformation.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using GalaSoft.MvvmLight;

namespace Dinkkkiia.WorkandPlay.Common.Models
{
    /// <summary>
    /// Holds information about a connected reader
    /// </summary>
    public class ReaderInformation
        : ObservableObject
    {
        /// <summary>
        /// Represents a blank version response
        /// </summary>
        public const string EmptyVersion = "v0.0.0";

        /// <summary>
        /// Backing field for <see cref="Model"/>
        /// </summary>
        private string model = string.Empty;

        /// <summary>
        /// Backing field for <see cref="ImageUri"/>
        /// </summary>
        private string imageUri = string.Empty;

        /// <summary>
        /// Backing field for <see cref="SerialNumber"/>
        /// </summary>
        private string serialNumber = string.Empty;

        /// <summary>
        /// Backing field for <see cref="RadioSerialNumber"/>
        /// </summary>
        private string radioSerialNumber = string.Empty;

        /// <summary>
        /// Backing field for <see cref="AntennaSerialNumber"/>
        /// </summary>
        private string antennaSerialNumber = string.Empty;

        /// <summary>
        /// Backing field for <see cref="Firmware"/>
        /// </summary>
        private string firmware = EmptyVersion;

        /// <summary>
        /// Backing field for <see cref="Bootloader"/>
        /// </summary>
        private string bootloader = EmptyVersion;

        /// <summary>
        /// Backing field for <see cref="RadioFirmware"/>
        /// </summary>
        private string radioFirmware = EmptyVersion;

        /// <summary>
        /// Backing field for <see cref="RadioBootloader"/>
        /// </summary>
        private string radioBootloader = EmptyVersion;

        /// <summary>
        /// Backing field for <see cref="Battery"/>
        /// </summary>
        private int battery = 0;

        /// <summary>
        /// Backing field for <see cref="Date"/>
        /// </summary>
        private DateTime date = DateTime.MinValue;

        /// <summary>
        /// Backing field for <see cref="Time"/>
        /// </summary>
        private DateTime time = DateTime.MinValue;

        /// <summary>
        /// Backing field for <see cref="CarrierPower"/>
        /// </summary>
        private int carrierPower = 29;

        /// <summary>
        /// Backing field for <see cref="MinimumCarrierPower"/>
        /// </summary>
        private int minimumCarrierPower = 10;

        /// <summary>
        /// Backing field for <see cref="MaximumCarrierPower"/>
        /// </summary>
        private int maximumCarrierPower = 30;

        /// <summary>
        /// Gets or sets the model number of the connected reader
        /// </summary>
        public string Model
        {
            get
            {
                return this.model;
            }

            set
            {
                this.Set(ref this.model, value);
            }
        }

        /// <summary>
        /// Gets or sets a URI for an image that represents the connected model
        /// </summary>
        public string ImageUri
        {
            get
            {
                return this.imageUri;
            }

            set
            {
                this.Set(ref this.imageUri, value);
            }
        }

        /// <summary>
        /// Gets or sets the serial number of the connected reader
        /// </summary>
        public string SerialNumber
        {
            get
            {
                return this.serialNumber;
            }

            set
            {
                this.Set(ref this.serialNumber, value);
            }
        }

        /// <summary>
        /// Gets or sets the radio serial number of the connected reader
        /// </summary>
        public string RadioSerialNumber
        {
            get
            {
                return this.radioSerialNumber;
            }

            set
            {
                this.Set(ref this.radioSerialNumber, value);
            }
        }

        /// <summary>
        /// Gets or sets the antenna serial number of the connected reader
        /// </summary>
        public string AntennaSerialNumber
        {
            get
            {
                return this.antennaSerialNumber;
            }

            set
            {
                this.Set(ref this.antennaSerialNumber, value);
            }
        }

        /// <summary>
        /// Gets or sets the firmware version of the connected reader
        /// </summary>
        public string Firmware
        {
            get
            {
                return this.firmware;
            }

            set
            {
                this.Set(ref this.firmware, value);
            }
        }

        /// <summary>
        /// Gets or sets the bootloader version of the connected reader
        /// </summary>
        public string Bootloader
        {
            get
            {
                return this.bootloader;
            }

            set
            {
                this.Set(ref this.bootloader, value);
            }
        }

        /// <summary>
        /// Gets or sets the maximum carrier power
        /// </summary>
        public int MaximumCarrierPower
        {
            get
            {
                return this.maximumCarrierPower;
            }

            set
            {
                this.Set(ref this.maximumCarrierPower, value);
            }
        }

        /// <summary>
        /// Gets or sets the minimum carrier power
        /// </summary>
        public int MinimumCarrierPower
        {
            get
            {
                return this.minimumCarrierPower;
            }

            set
            {
                this.Set(ref this.minimumCarrierPower, value);
            }
        }

        /// <summary>
        /// Gets or sets the radio firmware version of the connected reader
        /// </summary>
        public string RadioFirmware
        {
            get
            {
                return this.radioFirmware;
            }

            set
            {
                this.Set(ref this.radioFirmware, value);
            }
        }

        /// <summary>
        /// Gets or sets the radio bootloader version of the connected reader
        /// </summary>
        public string RadioBootloader
        {
            get
            {
                return this.radioBootloader;
            }

            set
            {
                this.Set(ref this.radioBootloader, value);
            }
        }

        /// <summary>
        /// Gets or sets the battery level of the connected reader
        /// </summary>
        public int Battery
        {
            get
            {
                return this.battery;
            }

            set
            {
                this.Set(ref this.battery, value);
            }
        }

        /// <summary>
        /// Gets or sets the date of the connected reader
        /// </summary>
        public DateTime Date
        {
            get
            {
                return this.date;
            }

            set
            {
                this.Set(ref this.date, value);
            }
        }

        /// <summary>
        /// Gets or sets the time of the connected reader
        /// </summary>
        public DateTime Time
        {
            get
            {
                return this.time;
            }

            set
            {
                this.Set(ref this.time, value);
            }
        }

        /// <summary>
        /// Gets or sets the carrier power of the connected reader
        /// </summary>
        public int CarrierPower
        {
            get
            {
                return this.carrierPower;
            }

            set
            {
                this.Set(ref this.carrierPower, value);
            }
        }        
    }
}
