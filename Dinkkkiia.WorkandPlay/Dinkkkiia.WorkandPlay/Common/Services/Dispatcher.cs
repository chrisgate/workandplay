﻿//-----------------------------------------------------------------------
// <copyright file="Dispatcher.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using Dinkkkiia.WorkandPlay.Common.ViewModels;

namespace Dinkkkiia.WorkandPlay.Common.Services
{
    /// <summary>
    /// An implementation of <see cref="IDispatcher"/> for Xamarin Forms
    /// </summary>
    public class Dispatcher
        : IDispatcher
    {
        /// <summary>
        /// Perform action on the user interface thread (either because this is already called from that thread of marshal the call)
        /// </summary>
        /// <param name="action">The action to perform on the user interface thread</param>
        /// <remarks>
        /// This behaves as BeginInvoke rather than Invoke (i.e. can return before the method completes)
        /// </remarks>
        public void InvokeOnUserInterfaceThread(Action action)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(action);
        }
    }
}
