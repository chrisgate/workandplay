﻿//-----------------------------------------------------------------------
// <copyright file="ReaderChangeNotification.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using GalaSoft.MvvmLight.Messaging;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.Common.Services
{
    /// <summary>
    /// Provides a mechanism to consume and announce to the application that the reader has connected or disconnected
    /// </summary>
    /// <remarks>
    /// Uses the default <see cref="IMessenger"/> to send a message to all instances of this class using the static 
    /// <see cref="ReaderConnected(VersionInformationCommand) "/> and <see cref="ReaderDisconnecting "/> methods
    /// </remarks>
    public class ReaderChangeNotification
        : IDisposable
    {
        /// <summary>
        /// True once an instance of this
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Backing field for IsConnected
        /// </summary>
        private bool? isConnected;

        /// <summary>
        /// This action gets invoked when the reader is connected
        /// </summary>
        private Action<VersionInformationCommand> configureReader;

        /// <summary>
        /// This action get invoked just before the reader is disconnected
        /// </summary>
        private Action beforeDisconnect;

        /// <summary>
        /// Initializes a new instance of the ReaderChangeNotification class
        /// </summary>
        /// <param name="configureReader">The method to call when a reader connects</param>
        /// <param name="beforeDisconnect">the method to call just before a reader disconnects</param>
        public ReaderChangeNotification(Action<VersionInformationCommand> configureReader, Action beforeDisconnect)
        {
            this.configureReader = configureReader;
            this.beforeDisconnect = beforeDisconnect;

            Messenger.Default.Register<GenericMessage<VersionInformationCommand>>(this, this.MessageReceived);
        }

        /// <summary>
        /// Gets a value indicating whether the reader is connected
        /// </summary>
        /// <remarks>
        /// Until the instance has received a notification this returns null as the state is unknown
        /// </remarks>
        public bool? IsConnected
        {
            get
            {
                return this.isConnected;
            }

            private set
            {
                this.isConnected = value;
            }
        }

        /// <summary>
        /// Calling this method announces to all instances that a new reader is connected and this is its version
        /// </summary>
        /// <param name="version">The response the reader gave to its version</param>
        public static void ReaderConnected(VersionInformationCommand version)
        {
            if (version == null)
            {
                // we can't send a null version as this is disconnect method
                throw new ArgumentNullException("version");
            }

            Messenger.Default.Send(new GenericMessage<VersionInformationCommand>(version));
        }

        /// <summary>
        /// Calling this method announces to all instances that the reader is about to disconnect
        /// </summary>
        public static void ReaderDisconnecting()
        {
            Messenger.Default.Send(new GenericMessage<VersionInformationCommand>(null));
        }

        /// <summary>
        /// Disposes an instance of the ReaderChangeNotification class
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes an instance of the ReaderChangeNotification class
        /// </summary>
        /// <param name="disposing">True to dispose managed as well as native resources</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Messenger.Default.Unregister(this);
                }

                this.disposed = true;
            }
        }

        /// <summary>
        /// Called when the <see cref="VersionInformationCommand "/> <see cref="GenericMessage{T}"/> is received (by one of the static methods.
        /// Calls the appropriate delegate
        /// </summary>
        /// <param name="message">The message received by the instance</param>
        private void MessageReceived(GenericMessage<VersionInformationCommand> message)
        {
            if (message.Content != null)
            {
                this.IsConnected = true;
                this.configureReader(message.Content);
            }
            else
            {
                this.IsConnected = false;
                this.beforeDisconnect();
            }
        }
    }
}
