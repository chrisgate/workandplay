﻿//-----------------------------------------------------------------------
// <copyright file="ReaderInformationProvider.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using Dinkkkiia.WorkandPlay.Common.Models;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;
using TechnologySolutions.Rfid.AsciiProtocol.Extensions;

namespace Dinkkkiia.WorkandPlay.Common.Services
{
    /// <summary>
    /// Updates a <see cref="ReaderInformation"/> when a reader connects
    /// </summary>
    public class ReaderInformationProvider
    {
        /// <summary>
        /// Used to communicate with the reader
        /// </summary>
        private IAsciiCommander commander;

        /// <summary>
        /// Notifies when a reader connection state changes
        /// </summary>
        private IReaderConnectionManager readerConnectionManager;

        /// <summary>
        /// The information to update
        /// </summary>
        private ReaderInformation information;

        /// <summary>
        /// Initializes a new instance of the ReaderInformationProvider class
        /// </summary>
        /// <param name="readerConnectionManager">Used to listen to connection changes</param>
        /// <param name="commander">Used to command the connected reader</param>
        /// <param name="information">The information to update</param>
        public ReaderInformationProvider(IReaderConnectionManager readerConnectionManager, IAsciiCommander commander, ReaderInformation information)
        {
            this.commander = commander;
            this.information = information;
            this.readerConnectionManager = readerConnectionManager;
            this.readerConnectionManager.ConnectionStateChanged += this.ReaderConnectionManager_ConnectionStateChanged;
        }

        /// <summary>
        /// Listens to events from the connection manager and updates the information as a new reader connects
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">Data provider for the event</param>
        private void ReaderConnectionManager_ConnectionStateChanged(object sender, EventArgs e)
        {
            // TODO: view model locator should register AsciiCommander to provide direct access to IAsciiserialTransportConsumer
            var transport = this.commander as IAsciiSerialTransportConsumer;

            switch (this.readerConnectionManager.ConnectionState)
            {
                case ReaderConnectionState.Disconnecting:
                    if (transport != null && transport.Transport != null)
                    {
                        // tell the application the reader is about to disconnect
                        ReaderChangeNotification.ReaderDisconnecting();
                        transport.Transport = null;
                    }

                    break;

                case ReaderConnectionState.Connected:
                    if (transport != null)
                    {
                        transport.Transport = this.readerConnectionManager.ConnectionTransport;
                    }

                    break;
            }

            if (ReaderConnectionState.Connected == this.readerConnectionManager.ConnectionState && this.readerConnectionManager.ConnectedReader != null)
            {
                for (int retry = 0; retry < 3; retry++)
                {
                    this.commander.Execute(new AbortCommand());
                    this.commander.Execute(new FactoryDefaultsCommand());

                    var version = new VersionInformationCommand();
                    this.commander.Execute(version);
                    if (version.Response.IsSuccessful)
                    {
                        // Confgiure the library to be specific to the connected reader
                        LibraryConfiguration.Current = LibraryConfiguration.ConfigurationForDevice(version.SerialNumber, new Version(version.FirmwareVersion));

                        // Update the reader information
                        this.UpdateFromVersion(version);

                        // tell the rest of the app now is the time to configure the newly connected reader
                        ReaderChangeNotification.ReaderConnected(version);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the information from version
        /// </summary>
        /// <param name="version">The version to update the information with</param>
        private void UpdateFromVersion(VersionInformationCommand version)
        {
            this.information.AntennaSerialNumber = version.AntennaSerialNumber;
            //// this.information.Battery = ;
            this.information.Bootloader = version.BootloaderVersion;
            //// this.information.CarrierPower =;
            //// this.information.Date = ;
            this.information.Firmware = version.FirmwareVersion;
            //// this.information.ImageUri = ;
            this.information.Model = this.ModelFromSerialNumber(version.SerialNumber);
            this.information.RadioBootloader = version.RadioBootloaderVersion;
            this.information.RadioFirmware = version.RadioFirmwareVersion;
            this.information.RadioSerialNumber = version.RadioSerialNumber;
            this.information.SerialNumber = version.SerialNumber;
            //// this.information.Time = ;                

            this.information.MaximumCarrierPower = LibraryConfiguration.Current.MaximumOutputPower;
            this.information.MinimumCarrierPower = LibraryConfiguration.Current.MinimumOutputPower;
        }

        /// <summary>
        /// Returns a model number from a serial number
        /// </summary>
        /// <param name="serialNumber">The reader serial number</param>
        /// <returns>The reader model number</returns>
        private string ModelFromSerialNumber(string serialNumber)
        {
            if (!string.IsNullOrEmpty(serialNumber) && serialNumber.Length >= 4)
            {
                return serialNumber.Substring(0, 4);
            }
            else
            {
                return "Unknown";
            }
        }
    }
}
