﻿//-----------------------------------------------------------------------
// <copyright file="IMonitorTransponders.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using Dinkkkiia.WorkandPlay.Models;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Notifies the progress of a command that returns transponders
    /// </summary>
    public interface IMonitorTransponders
    {
        /// <summary>
        /// Raised for each transponder in the response
        /// </summary>
        event EventHandler<TranspondersEventArgs> TranspondersReceived;

        /// <summary>
        /// Gets or sets a value indicating whether transponders should be reported
        /// </summary>
        bool IsEnabled { get; set; }
    }
}
