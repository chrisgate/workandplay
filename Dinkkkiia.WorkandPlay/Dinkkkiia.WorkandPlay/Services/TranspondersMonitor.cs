﻿//-----------------------------------------------------------------------
// <copyright file="TranspondersMonitor.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Dinkkkiia.WorkandPlay.Models;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Adapts a transponder command (that may be in a responder chain) to raise events
    /// as transponders are received
    /// </summary>
    public class TranspondersMonitor
        : IMonitorTransponders, IAsciiCommandResponder
    {
        /// <summary>
        /// The command in the responder chain
        /// </summary>
        private TranspondersCommandBase transponderCommand;

        /// <summary>
        /// The cache of transponders to report in the next batch
        /// </summary>
        private List<TransponderData> cache = new List<TransponderData>();

        /// <summary>
        /// The timestamp transponders were last reported
        /// </summary>
        private DateTime lastSentTimestamp = DateTime.Now;

        /// <summary>
        /// Initializes a new instance of the TranspondersMonitor class
        /// </summary>
        /// <param name="transponderCommand">The command to monitor for transponder events</param>
        public TranspondersMonitor(TranspondersCommandBase transponderCommand)
        {
            this.IsEnabled = true;

            if (transponderCommand == null)
            {
                throw new ArgumentNullException("transponderCommand");
            }

            this.transponderCommand = transponderCommand;
            this.transponderCommand.TransponderReceived += this.Transponders_TransponderReceived;
            this.transponderCommand.Response.CommandComplete += this.Transponders_CommandComplete;
        }

        /// <summary>
        /// Raised for each transponder in the response
        /// </summary>
        public event EventHandler<TranspondersEventArgs> TranspondersReceived;

        /// <summary>
        /// Gets or sets a value indicating whether transponders should be reported
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Returns a TranspondersMonitor instance that will notify of transponders inventoried
        /// </summary>
        /// <returns>The transpondersMonitor instance</returns>
        public static TranspondersMonitor ForInventory()
        {
            return new TranspondersMonitor(new InventoryCommand());
        }

        /// <summary>
        /// Consumes the transponder responses from the responder chain
        /// </summary>
        /// <param name="line">The line to be processed</param>
        /// <param name="moreLinesAvailable">When true indicates there are additional lines to be processed (and will also be passed to this method)</param>
        /// <returns>True if this line should not be passed to any other responder</returns>
        bool IAsciiCommandResponder.ProcessReceivedLine(IAsciiResponseLine line, bool moreLinesAvailable)
        {
            return this.transponderCommand.Responder.ProcessReceivedLine(line, moreLinesAvailable);
        }

        /// <summary>
        /// Raises the Transponder event
        /// </summary>
        /// <param name="transponders">The transponders to report</param>
        /// <param name="endOfPass">True if no more transponders are expected</param>
        protected virtual void OnTranspondersReceived(IEnumerable<TransponderData> transponders, bool endOfPass)
        {
            this.TranspondersReceived?.Invoke(this, new TranspondersEventArgs() { Transponders = transponders, EndOfPass = endOfPass });
        }

        /// <summary>
        /// Empties the cache of transponders to report and returns the contents
        /// </summary>
        /// <returns>The transponders to report from the cache</returns>
        private IEnumerable<TransponderData> EmptyCache()
        {
            var result = this.cache;
            this.cache = new List<TransponderData>();
            return result;
        }

        /// <summary>
        /// Handles a single transponder reported from the inventory
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">Data provided for the event</param>
        private void Transponders_TransponderReceived(object sender, TransponderDataEventArgs e)
        {
            this.cache.Add(e.Transponder);

            if (this.IsEnabled)
            {
                if (!e.MoreAvailable || this.cache.Count > 7 || DateTime.Now.Subtract(this.lastSentTimestamp).TotalMilliseconds > 300)
                {
                    this.OnTranspondersReceived(this.EmptyCache(), false);
                }
            }
        }

        /// <summary>
        /// Handles the completion of an inventory
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">Data provided for the event</param>
        private void Transponders_CommandComplete(object sender, EventArgs e)
        {
            if (this.IsEnabled)
            {
                this.OnTranspondersReceived(this.EmptyCache(), true);
            }
        }        
    }
}
