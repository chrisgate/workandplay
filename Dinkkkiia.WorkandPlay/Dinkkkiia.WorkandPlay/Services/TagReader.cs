﻿//-----------------------------------------------------------------------
// <copyright file="TagReader.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// A class to read tags
    /// This class demonstrates the use of the ASCII2 "ReadTransponderCommand"/>
    /// </summary>
    public class TagReader : ITagReader
    {
        /// <summary>
        /// count of transponders seen
        /// </summary>
        private int transpondersSeenCount = 0;

        /// <summary>
        /// The commander used to issue commands to the reader
        /// </summary>
        private IAsciiCommander commander;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagReader"/> class
        /// </summary>
        /// <param name="commander">the AsciiCommander to use to command the reader </param>
        public TagReader(IAsciiCommander commander)
        {
            this.commander = commander;
        }

        /// <summary>
        /// Raised as the read operation progresses to provide informational messages
        /// </summary>
        public event EventHandler<ProgressEventArgs> ProgressUpdate;

        /// <summary>
        /// Read tags
        /// </summary>
        /// <param name="hexIdentifier">EPC identifier filter as lower-case hex</param>
        /// <param name="memoryBank">which tag memory bank to read (0-3)</param>
        /// <param name="wordAddress">data offset into the selected bank</param>
        /// <param name="wordCount">words to read from the selected bank</param>
        /// <param name="outputPower">reader power setting</param>
        /// <returns>true if the operation succeeded</returns>
        public bool ReadTags(string hexIdentifier, int memoryBank, int wordAddress, int wordCount, int outputPower)
        {
            this.transpondersSeenCount = 0;

            this.OnProgressUpdated("Scanning for tags...");

            ReadTransponderCommand readCommand = new ReadTransponderCommand();

            // Ensure the reader has default values for any parameter that is not explicitly set below
            readCommand.ResetParameters = true;

            // set the reader power
            readCommand.OutputPower = outputPower;

            if (hexIdentifier.Length > 0)
            {
                // Set the parameters for the filter
                readCommand.SelectBank = Databank.ElectronicProductCode;
                readCommand.SelectData = hexIdentifier;
                readCommand.SelectLength = hexIdentifier.Length * 4;    // NB 2 hex digits == 8 bits
                readCommand.SelectOffset = 0x20;

                // Select matching transponders into the B state
                readCommand.SelectAction = SelectAction.DeassertSetBNotAssertSetA;
                readCommand.SelectTarget = SelectTarget.S1;

                // Query for B state to ensure only tags that actively responded are included
                readCommand.QuerySelect = QuerySelect.All;
                readCommand.QuerySession = QuerySession.S1;
                readCommand.QueryTarget = QueryTarget.TargetB;
            }

            // Read data from the chosen part of the selected bank
            switch (memoryBank)
            {
                case 0: readCommand.Bank = Databank.Reserved;
                        break;
                case 1: readCommand.Bank = Databank.ElectronicProductCode;
                        break;
                case 2: readCommand.Bank = Databank.TransponderIdentifier;
                        break;
                case 3: readCommand.Bank = Databank.User;
                        break;
            }

            readCommand.Offset = wordAddress;
            readCommand.Length = wordCount;

            // Process each response as it comes in
            readCommand.TransponderReceived += this.ReadCommand_TransponderReceived;

            // Execute the command
            this.commander.Execute(readCommand);

            this.OnProgressUpdated(string.Format("Transponders seen = " + this.transpondersSeenCount.ToString()));

            return true;
        }

        /// <summary>
        /// Notify changes to Progress
        /// </summary>
        /// <param name="message">the message included with the notification</param>
        protected virtual void OnProgressUpdated(string message)
        {
            EventHandler<ProgressEventArgs> handler;

            handler = this.ProgressUpdate;
            if (handler != null)
            {
                handler(this, new ProgressEventArgs(message));
            }
        }

        /// <summary>
        /// Report details about each transponder received from the Read command
        /// </summary>
        /// <param name="sender">the source of the event</param>
        /// <param name="e">the transponder data</param>
        private void ReadCommand_TransponderReceived(object sender, TransponderDataEventArgs e)
        {
            this.OnProgressUpdated(string.Format("EPC: {0}", e.Transponder.Epc));
            this.transpondersSeenCount++;

            if (!string.IsNullOrEmpty(e.Transponder.ReadData))
            {
                // show the data that was read
                this.OnProgressUpdated(string.Format("DATA: {0}", e.Transponder.ReadData));
            }

            this.OnProgressUpdated(string.Empty);
        }
    }
}
