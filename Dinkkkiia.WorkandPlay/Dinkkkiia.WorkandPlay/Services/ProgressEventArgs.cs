﻿//-----------------------------------------------------------------------
// <copyright file="ProgressEventArgs.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Class for progress event parameters
    /// </summary>
    public class ProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressEventArgs" /> class.
        /// </summary>
        /// <param name="message">the progress message</param>
        public ProgressEventArgs(string message)
        {
            this.ProgressMessage = message;
        }

        /// <summary>
        /// Gets the progress message
        /// </summary>
        public string ProgressMessage { get; }
    }
}
