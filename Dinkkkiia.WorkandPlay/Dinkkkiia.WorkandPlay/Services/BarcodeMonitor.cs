﻿//-----------------------------------------------------------------------
// <copyright file="BarcodeMonitor.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Provides a responder to listen for barcode events. Updates a BarcodeInventory with asynchronous barcodes received
    /// </summary>
    public class BarcodeMonitor
        : IMonitorBarcode, IAsciiCommandResponder
    {
        /// <summary>
        /// The command used to receive barcode responses
        /// </summary>
        private BarcodeCommand barcode;

        /// <summary>
        /// Initializes a new instance of the BarcodeMonitor class
        /// </summary>
        public BarcodeMonitor()
        {
            this.IsEnabled = true;

            this.barcode = new BarcodeCommand();
            this.barcode.BarcodeReceived += (sender, e) =>
            {
                if (this.IsEnabled)
                {
                    this.OnBarcodeReceived(e);
                }
            };
        }

        /// <summary>
        /// Raised when a barcode is received
        /// </summary>
        public event EventHandler<BarcodeEventArgs> BarcodeScanned;

        /// <summary>
        /// Gets or sets a value indicating whether barcodes should be reported
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Consumes the barcode responses from the responder chain
        /// </summary>
        /// <param name="line">The line to be processed</param>
        /// <param name="moreLinesAvailable">When true indicates there are additional lines to be processed (and will also be passed to this method)</param>
        /// <returns>True if this line should not be passed to any other responder</returns>
        bool IAsciiCommandResponder.ProcessReceivedLine(IAsciiResponseLine line, bool moreLinesAvailable)
        {
            return this.barcode.Responder.ProcessReceivedLine(line, moreLinesAvailable);
        }

        /// <summary>
        /// Raises the <see cref="BarcodeReceived"/> event
        /// </summary>
        /// <param name="e">Data provided for the barcode event</param>
        protected virtual void OnBarcodeReceived(BarcodeEventArgs e)
        {
            EventHandler<BarcodeEventArgs> handler;

            handler = this.BarcodeScanned;
            if (handler != null)
            {
                handler(this, e);
            }
        }        
    }
}
