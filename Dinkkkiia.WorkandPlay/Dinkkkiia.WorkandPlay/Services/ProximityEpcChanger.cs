﻿//-----------------------------------------------------------------------
// <copyright file="ProximityEpcChanger.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// A class to change the EPC of a single tag located by proximity
    /// This class demonstrates the use of the following ASCII 2 commands
    ///     <see cref="InventoryCommand"/>
    ///     <see cref="WriteSingleTransponderCommand"/>
    ///     <see cref="ReadTransponderCommand"/>
    /// </summary>
    public class ProximityEpcChanger : IEpcChanger
    {
        /// <summary>
        /// The single target tag must return a signal stronger than this.
        /// This will increase the probability that the detected tag is the closest to the reader
        /// </summary>
        private static int minimumAcceptableRssi = -50;

        /// <summary>
        /// The commander used to issue commands to the reader
        /// </summary>
        private IAsciiCommander commander;

        /// <summary>
        /// The hex identifier of the tag that is or was changed
        /// </summary>
        private string targetTagHexIdentifier;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProximityEpcChanger"/> class
        /// </summary>
        /// <param name="commander">the AsciiCommander to use to command the reader </param>
        public ProximityEpcChanger(IAsciiCommander commander)
        {
            this.commander = commander;
        }

        /// <summary>
        /// Raised as the Change EPC operation progresses to provide informational messages
        /// </summary>
        public event EventHandler<ProgressEventArgs> ProgressUpdate;

        /// <summary>
        /// Gets the identifier of the tag that is to be, or has been, modified
        /// </summary>
        public string TargetTagHexIdentifier
        {
            get
            {
                return this.targetTagHexIdentifier;
            }
        }

        /// <summary>
        /// Performs a sequence of operations attempting to change the EPC of the single, nearest
        /// tag to the given new identifier
        /// The length of the tag EPC will be adjusted to match the given identifier's length
        /// </summary>
        /// <param name="hexIdentifier">the new EPC identifier as lower-case hex - must be multiple of 4  digits long</param>
        /// <returns>true if the operation succeeded</returns>
        public bool ChangeEpcTo(string hexIdentifier)
        {
            this.targetTagHexIdentifier = null;

            this.OnProgressUpdated("Scanning for tag to modify...");

            //
            // Scan for tags in range
            //
            InventoryCommand inventoryCommand = new InventoryCommand();

            // Ensure the reader has default values for any parameter that is not explicitly set below
            inventoryCommand.ResetParameters = true;

            // Get the tag RSSI to estimate proximity to the reader
            inventoryCommand.IncludeTransponderRssi = TriState.Yes;

            // Use lower power to ignore transponders that are distant from the reader
            inventoryCommand.OutputPower = Math.Min(22, LibraryConfiguration.Current.MaximumOutputPower);

            // Get the PC word so that we can alter the EPC identifier length
            inventoryCommand.IncludePC = TriState.Yes;

            // Do the operation silently
            inventoryCommand.UseAlert = TriState.No;

            // Execute the command
            this.commander.Execute(inventoryCommand);

            List<TransponderData> transponders = new List<TransponderData>(inventoryCommand.Transponders);

            // Ensure that only one tag is in range
            if (transponders.Count != 1)
            {
                if (transponders.Count == 0)
                {
                    this.OnProgressUpdated("No tag found!");
                }
                else
                {
                    this.OnProgressUpdated(string.Format("Multiple tags ({0}) present!", transponders.Count));
                }

                return false;
            }

            // One tag was detected check that the PC word and rssi are valid
            // and try to ensure that it is close to the reader 
            TransponderData targetTag = transponders[0];

            this.OnProgressUpdated(string.Format("Tag found: {0}", targetTag.Epc));

            if (!targetTag.Pc.HasValue)
            {
                this.OnProgressUpdated("Unable to read Tag PC word!");
                return false;
            }

            if (!targetTag.Rssi.HasValue)
            {
                this.OnProgressUpdated("Cannot verify that tag is close to the reader!");
                return false;
            }

            if (targetTag.Rssi.Value < ProximityEpcChanger.minimumAcceptableRssi)
            {
                this.OnProgressUpdated("Tag is too far from reader!");
                return false;
            }

            // Update the target tag property
            this.targetTagHexIdentifier = targetTag.Epc;

            //
            // Write the new EPC to the tag's EPC memory bank
            //
            WriteSingleTransponderCommand writeSingleCommand = new WriteSingleTransponderCommand();

            // Ensure the reader has default values for any parameter that is not explicitly set below
            writeSingleCommand.ResetParameters = true;

            // Perform action silently
            writeSingleCommand.UseAlert = TriState.No;

            // Set the singulation parameters
            writeSingleCommand.SelectBank = Databank.ElectronicProductCode;
            writeSingleCommand.SelectData = targetTag.Epc;
            writeSingleCommand.SelectLength = targetTag.Epc.Length * 4;
            writeSingleCommand.SelectOffset = 0x20;

            // Set the data to be written
            if (this.EpcLengthFromPc(targetTag.Pc.Value) == hexIdentifier.Length / 4)
            {
                // New EPC is same length as the old - just write the new EPC value
                writeSingleCommand.Data = hexIdentifier;
                writeSingleCommand.Bank = Databank.ElectronicProductCode;
                writeSingleCommand.Offset = 2; // Writing EPC value only 
                writeSingleCommand.Length = hexIdentifier.Length / 4;
            }
            else
            {
                // The PC word will need to be changed as the new length is different
                int pcWord = this.UpdatePcForLength(targetTag.Pc.Value, hexIdentifier.Length / 4);
                string pcAndIdentifier = string.Format("{0:x4}", pcWord) + hexIdentifier;

                writeSingleCommand.Data = pcAndIdentifier;
                writeSingleCommand.Bank = Databank.ElectronicProductCode;
                writeSingleCommand.Offset = 1; // Writing PC and EPC value 
                writeSingleCommand.Length = pcAndIdentifier.Length / 4;
            }

            // A single tag is being selected so full power can be used
            writeSingleCommand.OutputPower = LibraryConfiguration.Current.MaximumOutputPower;

            this.OnProgressUpdated(string.Empty);
            this.OnProgressUpdated("Changing EPC of tag...");

            // Execute the command
            this.commander.Execute(writeSingleCommand);

            if (writeSingleCommand.WordsWritten != writeSingleCommand.Length)
            {
                if (writeSingleCommand.WordsWritten == 0)
                {
                    this.OnProgressUpdated("Unable to write new EPC!");
                }
                else
                {
                    this.OnProgressUpdated(string.Format("Incomplete EPC written ({0})", writeSingleCommand.WordsWritten));
                }

                return false;
            }
            else
            {
                // Write succeeded
                this.OnProgressUpdated(string.Empty);
                this.OnProgressUpdated("EPC changed - verifying with TID read...");

                // Verify by reading back data from the tag using the new EPC value
                ReadTransponderCommand readCommand = new ReadTransponderCommand();

                // Ensure the reader has default values for any parameter that is not explicitly set below
                readCommand.ResetParameters = true;

                // Set the singulation parameters for the new EPC
                readCommand.SelectBank = Databank.ElectronicProductCode;
                readCommand.SelectData = hexIdentifier;
                readCommand.SelectLength = hexIdentifier.Length * 4;
                readCommand.SelectOffset = 0x20;

                // Select matching transponders into the B state (there should be only one!)
                readCommand.SelectAction = SelectAction.DeassertSetBNotAssertSetA;
                readCommand.SelectTarget = SelectTarget.S1;

                // Query for B state to ensure only tags that actively responded are included
                readCommand.QuerySelect = QuerySelect.All;
                readCommand.QuerySession = QuerySession.S1;
                readCommand.QueryTarget = QueryTarget.TargetB;

                // Read 2 words of data from the start of the TID bank
                // (this includes the Designer Id and the Model Id)
                readCommand.Bank = Databank.TransponderIdentifier;
                readCommand.Offset = 0;
                readCommand.Length = 2;

                // Process each response as it comes in
                readCommand.TransponderReceived += this.ReadCommand_TransponderReceived;

                this.OnProgressUpdated(string.Empty);
                this.OnProgressUpdated(string.Format("Reading TID from: {0}", hexIdentifier));
                this.OnProgressUpdated(string.Empty);

                // Execute the command
                this.commander.Execute(readCommand);
            }

            return true;
        }

        /// <summary>
        /// Notify changes to Progress
        /// </summary>
        /// <param name="message">the message included with the notification</param>
        protected virtual void OnProgressUpdated(string message)
        {
            EventHandler<ProgressEventArgs> handler;

            handler = this.ProgressUpdate;
            if (handler != null)
            {
                handler(this, new ProgressEventArgs(message));
            }
        }

        /// <summary>
        /// Report details about each transponder received from the Read command
        /// </summary>
        /// <param name="sender">the source of the event</param>
        /// <param name="e">the transponder data</param>
        private void ReadCommand_TransponderReceived(object sender, TransponderDataEventArgs e)
        {
            this.OnProgressUpdated(string.Format("EPC: {0}", e.Transponder.Epc));

            if (!string.IsNullOrEmpty(e.Transponder.ReadData) && e.Transponder.ReadData.Length >= 8)
            {
                // Always show the full TID data that was read
                this.OnProgressUpdated(string.Format("TID: {0}", e.Transponder.ReadData));

                // Extract the Designer & Model Ids when available
                if (e.Transponder.ReadData.Substring(0, 2).Equals("E2"))
                {
                    string designerId = e.Transponder.ReadData.Substring(2, 3);
                    string modelId = e.Transponder.ReadData.Substring(5, 3);
                    this.OnProgressUpdated(string.Format("Designer Id: {0}", designerId));
                    this.OnProgressUpdated(string.Format("Model Id: {0}", modelId));
                }
            }

            this.OnProgressUpdated(string.Empty);
        }

        /// <summary>
        /// Gets the new PC word value for the given length
        /// </summary>
        /// <param name="pc">the existing PC word</param>
        /// <param name="length">the new length</param>
        /// <returns>the modified PC word</returns>
        private int UpdatePcForLength(int pc, int length)
        {
            return (pc & 0x7ff) | length << 11;
        }

        /// <summary>
        /// Gets the EPC identifier length from the given PC word length 
        /// </summary>
        /// <param name="pc">the PC word</param>
        /// <returns>the EPC identifier length </returns>
        private int EpcLengthFromPc(int pc)
        {
            return (pc & 0xf800) >> 11;
        }
    }
}
