﻿//-----------------------------------------------------------------------
// <copyright file="IMonitorBarcode.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2014 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
// <author>Robin Stone</author>
//-----------------------------------------------------------------------

using System;
using TechnologySolutions.Rfid.AsciiProtocol;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Notifies when a barcode is received
    /// </summary>
    public interface IMonitorBarcode
    {
        /// <summary>
        /// Raised when a barcode scan is received from the reader
        /// </summary>
        event EventHandler<BarcodeEventArgs> BarcodeScanned;

        /// <summary>
        /// Gets or sets a value indicating whether barcodes should be reported
        /// </summary>
        bool IsEnabled { get; set; }
    }
}
