﻿//-----------------------------------------------------------------------
// <copyright file="InventoryConfigurator.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;
using System.Threading.Tasks;
using Dinkkkiia.WorkandPlay.Models;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Configures a reader for the specified inventory
    /// </summary>
    public class InventoryConfigurator
        : IInventoryConfigurator
    {
        /// <summary>
        /// The AsciiCommander used to execute commands
        /// </summary>
        private IAsciiCommander commander;

        /// <summary>
        /// the configuration to be used
        /// </summary>
        private InventoryConfiguration configuration;

        /// <summary>
        /// The command used to configure the reader inventory parameters
        /// </summary>
        private InventoryCommand configuringCommand;

        /// <summary>
        /// Initializes a new instance of the InventoryConfigurator class
        /// </summary>
        /// <param name="commander">the AsciiCommander to execute commands</param>
        /// <param name="configuration">Reference to the configuration to apply</param>
        public InventoryConfigurator(IAsciiCommander commander, InventoryConfiguration configuration)
        {
            this.commander = commander;
            this.configuration = configuration;

            this.configuringCommand = new InventoryCommand();
            this.configuringCommand.TakeNoAction = true;
            this.configuringCommand.ResetParameters = true;
        }

        /// <summary>
        /// Configures Inventory operations to respect the Configurator's current configuration parameters
        /// </summary>
        /// <returns>The task to configure the reader</returns>
        public async Task ConfigureAsync()
        {
            // The command is re-used so need to ensure parameters are 
            this.configuringCommand.IncludeTransponderRssi = this.configuration.IsRssiRequested ? TriState.Yes : TriState.No;
            this.configuringCommand.OutputPower = this.configuration.OutputPower;

            // Execute the command synchronously - if it fails user can re-send
            await Task.Run(() =>
            {
                try
                {
                    this.commander.Execute(this.configuringCommand);
                }
                catch (Exception ex)
                {
                    
                }
            });
        }
    }
}
