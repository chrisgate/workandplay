﻿//-----------------------------------------------------------------------
// <copyright file="IInventoryConfigurator.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Provides a method to configure a reader to match a <see cref="SportSmartTags.Models.InventoryConfiguration"/>
    /// </summary>
    public interface IInventoryConfigurator
    {
        /// <summary>
        /// Configures Inventory operations to respect the Configurator's current configuration parameters
        /// </summary>
        /// <returns>
        /// The task to configure the reader
        /// </returns>
        Task ConfigureAsync();
    }
}
