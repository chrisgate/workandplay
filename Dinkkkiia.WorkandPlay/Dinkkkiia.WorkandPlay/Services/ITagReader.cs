﻿//-----------------------------------------------------------------------
// <copyright file="ITagReader.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Defines an interface for reading tags
    /// </summary>
    public interface ITagReader
    {
        /// <summary>
        /// Raised as the read operation progresses to provide informational messages
        /// </summary>
        event EventHandler<ProgressEventArgs> ProgressUpdate;

        /// <summary>
        /// Attempts to read (filtered) tags
        /// <para />
        /// The length of the tag EPC will be adjusted to match the given identifier's length
        /// <para />
        /// </summary>
        /// <param name="hexIdentifier">EPC identifier filter as lower-case hex</param>
        /// <param name="memoryBank">which tag memory bank to read (0-3)</param>
        /// <param name="wordAddress">data offset into the selected bank</param>
        /// <param name="wordCount">words to read from the selected bank</param>
        /// <param name="outputPower">reader power setting</param>
        /// <returns>true if the operation succeeded</returns>
        bool ReadTags(string hexIdentifier, int memoryBank, int wordAddress, int wordCount, int outputPower);
    }
}
