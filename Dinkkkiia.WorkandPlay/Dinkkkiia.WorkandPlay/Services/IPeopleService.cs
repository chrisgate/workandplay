﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dinkkkiia.WorkandPlay.Models;

namespace Dinkkkiia.WorkandPlay.Services
{
    public interface IPeopleService
    {
        Task<IEnumerable<Person>> GetPeople(int count = 42);
    }
}
