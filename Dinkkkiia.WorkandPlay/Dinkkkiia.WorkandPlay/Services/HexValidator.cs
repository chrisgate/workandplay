﻿//-----------------------------------------------------------------------
// <copyright file="HexValidator.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System.Linq;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Provides validation of Hex strings
    /// </summary>
    public class HexValidator
    {
        /// <summary>
        /// Characters that can be used to make up the hex value
        /// </summary>
        private static char[] hexCharacters = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F' };

        /// <summary>
        /// Tests the value is valid Hex and multiple words
        /// </summary>
        /// <param name="value">the string to evaluate</param>
        /// <returns>true if the value represents multiple valid Hex words</returns>
        public static bool IsValidWordAlignedHex(string value)
        {
            // Null/empty are defined as invalid
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            // Must be word aligned
            if ((value.Length % 4) != 0)
            {
                return false;
            }

            // All digits must be hex values
            foreach (char character in value)
            {
                if (!hexCharacters.Contains(character))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
