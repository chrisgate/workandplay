﻿//-----------------------------------------------------------------------
// <copyright file="TagWriter.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;
using TechnologySolutions.Rfid.AsciiProtocol;
using TechnologySolutions.Rfid.AsciiProtocol.Commands;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// A class to read tags
    /// This class demonstrates the use of the following ASCII 2 commands
    ///     <see cref="WriteTransponderCommand"/>
    /// </summary>
    public class TagWriter : ITagWriter
    {
        /// <summary>
        /// count of transponders seen
        /// </summary>
        private int transpondersSeenCount = 0;

        /// <summary>
        /// The commander used to issue commands to the reader
        /// </summary>
        private IAsciiCommander commander;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagWriter"/> class
        /// </summary>
        /// <param name="commander">the AsciiCommander to use to command the reader </param>
        public TagWriter(IAsciiCommander commander)
        {
            this.commander = commander;
        }

        /// <summary>
        /// Raised as the read operation progresses to provide informational messages
        /// </summary>
        public event EventHandler<ProgressEventArgs> ProgressUpdate;

        /// <summary>
        /// Read tags
        /// </summary>
        /// <param name="hexIdentifier">EPC identifier filter as lower-case hex</param>
        /// <param name="memoryBank">which tag memory bank to read (0-3)</param>
        /// <param name="wordAddress">data offset into the selected bank</param>
        /// <param name="wordCount">words to read from the selected bank</param>
        /// <param name="hexData">Word-aligned data to write</param>
        /// <param name="outputPower">reader power setting</param>
        /// <returns>true if the operation succeeded</returns>
        public bool WriteTags(string hexIdentifier, int memoryBank, int wordAddress, int wordCount, string hexData, int outputPower)
        {
            this.transpondersSeenCount = 0;

            this.OnProgressUpdated("Updating tags...");

            WriteTransponderCommand writeCommand = new WriteTransponderCommand();

            // Ensure the reader has default values for any parameter that is not explicitly set below
            writeCommand.ResetParameters = true;

            // set the reader power
            writeCommand.OutputPower = outputPower;

            if (hexIdentifier.Length > 0)
            {
                // Set the parameters for the filter
                writeCommand.SelectBank = Databank.ElectronicProductCode;
                writeCommand.SelectData = hexIdentifier;
                writeCommand.SelectLength = hexIdentifier.Length * 4;    // NB 2 hex digits == 8 bits
                writeCommand.SelectOffset = 0x20;

                // Select matching transponders into the B state
                writeCommand.SelectAction = SelectAction.DeassertSetBNotAssertSetA;
                writeCommand.SelectTarget = SelectTarget.S1;

                // Query for B state to ensure only tags that actively responded are included
                writeCommand.QuerySelect = QuerySelect.All;
                writeCommand.QuerySession = QuerySession.S1;
                writeCommand.QueryTarget = QueryTarget.TargetB;
            }

            // Write data from the chosen part of the selected bank
            switch (memoryBank)
            {
                case 0: writeCommand.Bank = Databank.Reserved;
                        break;
                case 1: writeCommand.Bank = Databank.ElectronicProductCode;
                        break;
                case 2: writeCommand.Bank = Databank.TransponderIdentifier;
                        break;
                case 3: writeCommand.Bank = Databank.User;
                        break;
            }

            writeCommand.Data = hexData;
            writeCommand.Offset = wordAddress;
            writeCommand.Length = wordCount;

            // Process each response as it comes in
            writeCommand.TransponderReceived += this.WriteCommand_TransponderReceived;

            // Execute the command
            this.commander.Execute(writeCommand);

            this.OnProgressUpdated(string.Format("Transponders seen = " + this.transpondersSeenCount.ToString()));

            return true;
        }

        /// <summary>
        /// Notify changes to Progress
        /// </summary>
        /// <param name="message">the message included with the notification</param>
        protected virtual void OnProgressUpdated(string message)
        {
            EventHandler<ProgressEventArgs> handler;

            handler = this.ProgressUpdate;
            if (handler != null)
            {
                handler(this, new ProgressEventArgs(message));
            }
        }

        /// <summary>
        /// Report details about each transponder received from the Read command
        /// </summary>
        /// <param name="sender">the source of the event</param>
        /// <param name="e">the transponder data</param>
        private void WriteCommand_TransponderReceived(object sender, TransponderDataEventArgs e)
        {
            this.OnProgressUpdated(string.Format("EPC: {0}", e.Transponder.Epc));
            
            if (e.Transponder.WordsWritten != null)
            {
                this.OnProgressUpdated(string.Format("WRITTEN: {0} words", e.Transponder.WordsWritten));
            }

            this.transpondersSeenCount++;

            this.OnProgressUpdated(string.Empty);
        }
    }
}
