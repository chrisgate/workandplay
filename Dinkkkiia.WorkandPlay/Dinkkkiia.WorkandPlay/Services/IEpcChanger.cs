﻿//-----------------------------------------------------------------------
// <copyright file="IEpcChanger.cs" company="Technology Solutions UK Ltd"> 
//     Copyright (c) 2016 Technology Solutions UK Ltd. All rights reserved. 
// </copyright> 
//-----------------------------------------------------------------------

using System;

namespace Dinkkkiia.WorkandPlay.Services
{
    /// <summary>
    /// Defines an interface for changing the EPC of a tag
    /// </summary>
    public interface IEpcChanger
    {
        /// <summary>
        /// Raised as the Change EPC operation progresses to provide informational messages
        /// </summary>
        event EventHandler<ProgressEventArgs> ProgressUpdate;

        /// <summary>
        /// Gets the identifier of the tag that is to be, or has been, modified
        /// </summary>
        string TargetTagHexIdentifier { get; }

        /// <summary>
        /// Performs a sequence of operations attempting to change the EPC of the current target tag
        /// to the given new identifier
        /// <para />
        /// The length of the tag EPC will be adjusted to match the given identifier's length
        /// <para />
        /// </summary>
        /// <param name="hexIdentifier">the new EPC identifier - must be multiple of 4 hex digits long</param>
        /// <returns>true if the operation succeeded</returns>
        bool ChangeEpcTo(string hexIdentifier);
    }
}
